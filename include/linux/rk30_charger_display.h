#ifndef    _RK_CHARGE_DISPLAY_H
#define    _RK_CHARGE_DISPLAY_H
#include <linux/linux_logo.h>


#define LOGO_X  300 //CONFIG_CHARGE_LOGO_X //220
#define LOGO_Y  224 //CONFIG_CHARGE_LOGO_Y //110
extern struct timer_list blctl_timer;
extern int timer_cunt;
extern int charge_disp_mode ;
extern struct list_head rk_psy_head;
extern void rk30_backlight_ctl(int brightness);
extern void rk30_backlight_set(bool on);
#endif
