﻿/* 
 * drivers/input/touchscreen/ft5x0x_ts.c
 *
 * FocalTech ft5x0x TouchScreen driver. 
 *
 * Copyright (c) 2010  Focal tech Ltd.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *	note: only support mulititouch	Wenfs 2010-10-01
 */

#include <linux/input.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/slab.h>
#include <linux/fcntl.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/timer.h>
#include <linux/jiffies.h>
#include <linux/miscdevice.h>
#include <linux/types.h>
#include <linux/io.h>
#include <linux/input/mt.h>
#include <linux/delay.h>
#include <linux/ioport.h>
#include <linux/input-polldev.h>
#include <linux/i2c.h>
#include <linux/workqueue.h>
#ifdef CONFIG_ANDROID_POWER
#include <linux/android_power.h>
#endif
#include <mach/hardware.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/flash.h>
#include <asm/hardware/gic.h>
#include <mach/iomux.h>
#include <mach/gpio.h>
#include <mach/irqs.h>
#include <mach/board.h>
#include <media/soc_camera.h>                               /* ddl@rock-chips.com : camera support */
#include <linux/earlysuspend.h>
static struct early_suspend ft5406_power;



#if 0
#define DBG(x...) printk(KERN_INFO x)
#else
#define DBG(x...) do { } while (0)
#endif


#define CONFIG_FT5X0X_MULTITOUCH  1

#if defined(CONFIG_LCD_SC_N81S) || defined(CONFIG_LCD_SC_N71S) || defined(CONFIG_LCD_SC_N71SI)    //focaltech 5306 only support 5 points @ zpp 2012/7/5 13:42:41
#define MAX_POINT                 5
#else
#define MAX_POINT                 10
#endif

#define FT5406_IIC_SPEED          400*1000    //300*1000
//#define TOUCH_RESET_PIN           RK29_PIN6_PC3
#define FT5X0X_REG_THRES          0x80         /* Thresshold, the threshold be low, the sensitivy will be high */
#define FT5X0X_REG_REPORT_RATE    0x88         /* **************report rate, in unit of 10Hz **************/
#define FT5X0X_REG_PMODE          0xA5         /* Power Consume Mode 0 -- active, 1 -- monitor, 3 -- sleep */    
#define FT5X0X_REG_FIRMID         0xA6         /* ***************firmware version **********************/
#define FT5X0X_REG_NOISE_MODE     0xb2         /* to enable or disable power noise, 1 -- enable, 0 -- disable */
#define SCREEN_MAX_X              CONFIG_FOCALTECHTOUCH_MAX_X //1024
#define SCREEN_MAX_Y              CONFIG_FOCALTECHTOUCH_MAX_Y //768
#define PRESS_MAX                 255
#define FT5X0X_NAME	              "focaltech_ts"//"synaptics_i2c_rmi"//"synaptics-rmi-ts"// 
#define TOUCH_MAJOR_MAX           200
#define WIDTH_MAJOR_MAX           200
//FT5X0X_REG_PMODE
#define PMODE_ACTIVE              0x00
#define PMODE_MONITOR             0x01
#define PMODE_STANDBY             0x02
#define PMODE_HIBERNATE           0x03

struct ts_object {
    u8 id;	
	u16	x;
	u16	y;
	u8  status;	
	u16 pressure;

};
struct ts_event {
	u8  touch_point;
	struct ts_object ts_objects[MAX_POINT];
};

struct tp_event {
	u16	x;
	u16	y;
    s16 id;
	u16	pressure;
	u8  touch_point;
	u8  flag;
};

struct ft5x0x_ts_data {
	struct i2c_client *client;
	struct input_dev	*input_dev;
	int    irq;
	struct ts_event		event;
	struct work_struct 	pen_event_work;
	struct workqueue_struct *ts_workqueue;
};

static struct timer_list tp_timer;

static struct i2c_client *this_client;

/***********************************************************************/

#define    FTS_PACKET_LENGTH        128

extern int ft5x02_Init_IC_Param(struct i2c_client * client);
extern int ft5x02_get_ic_param(struct i2c_client * client);

static u8 CTPM_FW[]=
{
#if defined(CONFIG_LCD_SC_971S)
//#include "qtw_1024x768.i"
//#include "FT5406_RG097_Ver0x15_20121212_app"
#else 
	0x0
#endif
};

typedef enum
{
    ERR_OK,
    ERR_MODE,
    ERR_READID,
    ERR_ERASE,
    ERR_STATUS,
    ERR_ECC,
    ERR_DL_ERASE_FAIL,
    ERR_DL_PROGRAM_FAIL,
    ERR_DL_VERIFY_FAIL
}E_UPGRADE_ERR_TYPE;

/***********************************************************************/

/***********************************************************************
    [function]: 
		           callback:                send data to ctpm by i2c interface;
    [parameters]:
			    txdata[in]:              data buffer which is used to send data;
			    length[in]:              the length of the data buffer;
    [return]:
			    FTS_TRUE:              success;
			    FTS_FALSE:             fail;
************************************************************************/
static int fts_i2c_txdata(u8 *txdata, int length)
{
	int ret;

	struct i2c_msg msg;

      msg.addr = this_client->addr;
      msg.flags = 0;
      msg.len = length;
      msg.buf = txdata;
      msg.scl_rate = FT5406_IIC_SPEED;
	ret = i2c_transfer(this_client->adapter, &msg, 1);
	if (ret < 0)
		pr_err("%s i2c write error: %d\n", __func__, ret);

	return ret;
}

/***********************************************************************
    [function]: 
		           callback:               write data to ctpm by i2c interface;
    [parameters]:
			    buffer[in]:             data buffer;
			    length[in]:            the length of the data buffer;
    [return]:
			    FTS_TRUE:            success;
			    FTS_FALSE:           fail;
************************************************************************/
static bool  i2c_write_interface(u8* pbt_buf, int dw_lenth)
{
    int ret;
    ret=i2c_master_send(this_client, pbt_buf, dw_lenth);
    if(ret<=0)
    {
        //printk("[TSP]i2c_write_interface error line = %d, ret = %d\n", __LINE__, ret);
        return false;
    }

    return true;
}

/***********************************************************************
    [function]: 
		           callback:                read register value ftom ctpm by i2c interface;
    [parameters]:
                        reg_name[in]:         the register which you want to write;
			    tx_buf[in]:              buffer which is contained of the writing value;
    [return]:
			    FTS_TRUE:              success;
			    FTS_FALSE:             fail;
************************************************************************/
static bool fts_register_write(u8 reg_name, u8* tx_buf)
{
	u8 write_cmd[2] = {0};

	write_cmd[0] = reg_name;
	write_cmd[1] = *tx_buf;

	/*call the write callback function*/
	return i2c_write_interface(write_cmd, 2);
}

/***********************************************************************
[function]: 
                      callback:         send a command to ctpm.
[parameters]:
			  btcmd[in]:       command code;
			  btPara1[in]:     parameter 1;    
			  btPara2[in]:     parameter 2;    
			  btPara3[in]:     parameter 3;    
                      num[in]:         the valid input parameter numbers, 
                                           if only command code needed and no 
                                           parameters followed,then the num is 1;    
[return]:
			  FTS_TRUE:      success;
			  FTS_FALSE:     io fail;
************************************************************************/
static bool cmd_write(u8 btcmd,u8 btPara1,u8 btPara2,u8 btPara3,u8 num)
{
    u8 write_cmd[4] = {0};

    write_cmd[0] = btcmd;
    write_cmd[1] = btPara1;
    write_cmd[2] = btPara2;
    write_cmd[3] = btPara3;
    return i2c_write_interface(write_cmd, num);
}

/***********************************************************************
    [function]: 
		           callback:              read data from ctpm by i2c interface;
    [parameters]:
			    buffer[in]:            data buffer;
			    length[in]:           the length of the data buffer;
    [return]:
			    FTS_TRUE:            success;
			    FTS_FALSE:           fail;
************************************************************************/
static bool i2c_read_interface(u8* pbt_buf, int dw_lenth)
{
    int ret;
    
    ret=i2c_master_recv(this_client, pbt_buf, dw_lenth);

    if(ret<=0)
    {
        //printk("[TSP]i2c_read_interface error\n");
        return false;
    }
  
    return true;
}


/***********************************************************************
[function]: 
                      callback:         read a byte data  from ctpm;
[parameters]:
			  buffer[in]:       read buffer;
			  length[in]:      the size of read data;    
[return]:
			  FTS_TRUE:      success;
			  FTS_FALSE:     io fail;
************************************************************************/
static bool byte_read(u8* buffer, int length)
{
    return i2c_read_interface(buffer, length);
}

/***********************************************************************
[function]: 
                      callback:         write a byte data  to ctpm;
[parameters]:
			  buffer[in]:       write buffer;
			  length[in]:      the size of write data;    
[return]:
			  FTS_TRUE:      success;
			  FTS_FALSE:     io fail;
************************************************************************/
static bool byte_write(u8* buffer, int length)
{
    
    return i2c_write_interface(buffer, length);
}

/***********************************************************************
    [function]: 
		           callback:                 read register value ftom ctpm by i2c interface;
    [parameters]:
                        reg_name[in]:         the register which you want to read;
			    rx_buf[in]:              data buffer which is used to store register value;
			    rx_length[in]:          the length of the data buffer;
    [return]:
			    FTS_TRUE:              success;
			    FTS_FALSE:             fail;
************************************************************************/
static bool fts_register_read(u8 reg_name, u8* rx_buf, int rx_length)
{
	u8 read_cmd[2]= {0};
	u8 cmd_len 	= 0;

	read_cmd[0] = reg_name;
	cmd_len = 1;	

	/*send register addr*/
	if(!i2c_write_interface(&read_cmd[0], cmd_len))
	{
		return false;
	}

	/*call the read callback function to get the register value*/		
	if(!i2c_read_interface(rx_buf, rx_length))
	{
		return false;
	}
	return true;
}



/***********************************************************************
[function]: 
                        callback:          burn the FW to ctpm.
[parameters]:
			    pbt_buf[in]:     point to Head+FW ;
			    dw_lenth[in]:   the length of the FW + 6(the Head length);    
[return]:
			    ERR_OK:          no error;
			    ERR_MODE:      fail to switch to UPDATE mode;
			    ERR_READID:   read id fail;
			    ERR_ERASE:     erase chip fail;
			    ERR_STATUS:   status error;
			    ERR_ECC:        ecc error.
************************************************************************/
E_UPGRADE_ERR_TYPE  fts_ctpm_fw_upgrade(u8* pbt_buf, int dw_lenth)
{
    u8  cmd,reg_val[2] = {0};
	u8  buffer[2] = {0};
    u8  packet_buf[FTS_PACKET_LENGTH + 6];
    u8  auc_i2c_write_buf[10];
    u8  bt_ecc;
	
    int  j,temp,lenght,i_ret,packet_number, i = 0;
    int  i_is_new_protocol = 0;
	

    /******write 0xaa to register 0xfc******/
    cmd=0xaa;
    fts_register_write(0xfc,&cmd);
    mdelay(50);
	
     /******write 0x55 to register 0xfc******/
    cmd=0x55;
    fts_register_write(0xfc,&cmd);
    printk("[TSP] Step 1: Reset CTPM test\n");
   
    mdelay(10);   


    /*******Step 2:Enter upgrade mode ****/
    printk("\n[TSP] Step 2:enter new update mode\n");
    auc_i2c_write_buf[0] = 0x55;
    auc_i2c_write_buf[1] = 0xaa;
    do
    {
        i ++;
        i_ret = fts_i2c_txdata(auc_i2c_write_buf, 2);
        mdelay(5);
    }while(i_ret <= 0 && i < 10 );

    if (i > 1)
    {
        i_is_new_protocol = 1;
    }

    /********Step 3:check READ-ID********/        
    cmd_write(0x90,0x00,0x00,0x00,4);
    byte_read(reg_val,2);
    if (reg_val[0] == 0x79 && reg_val[1] == 0x3)
    {
        printk("[TSP] Step 3: CTPM ID,ID1 = 0x%x,ID2 = 0x%x\n",reg_val[0],reg_val[1]);
    }
    else
    {
        return ERR_READID;
        //i_is_new_protocol = 1;
    }
    

     /*********Step 4:erase app**********/
    if (i_is_new_protocol)
    {
        cmd_write(0x61,0x00,0x00,0x00,1);
    }
    else
    {
        cmd_write(0x60,0x00,0x00,0x00,1);
    }
    mdelay(1500);
    printk("[TSP] Step 4: erase. \n");



    /*Step 5:write firmware(FW) to ctpm flash*/
    bt_ecc = 0;
    printk("[TSP] Step 5: start upgrade. \n");
    dw_lenth = dw_lenth - 8;
    packet_number = (dw_lenth) / FTS_PACKET_LENGTH;
    packet_buf[0] = 0xbf;
    packet_buf[1] = 0x00;
    for (j=0;j<packet_number;j++)
    {
        temp = j * FTS_PACKET_LENGTH;
        packet_buf[2] = (u8)(temp>>8);
        packet_buf[3] = (u8)temp;
        lenght = FTS_PACKET_LENGTH;
        packet_buf[4] = (u8)(lenght>>8);
        packet_buf[5] = (u8)lenght;

        for (i=0;i<FTS_PACKET_LENGTH;i++)
        {
            packet_buf[6+i] = pbt_buf[j*FTS_PACKET_LENGTH + i]; 
            bt_ecc ^= packet_buf[6+i];
        }
        
        byte_write(&packet_buf[0],FTS_PACKET_LENGTH + 6);
        mdelay(FTS_PACKET_LENGTH/6 + 1);
        if ((j * FTS_PACKET_LENGTH % 1024) == 0)
        {
              printk("[TSP] upgrade the 0x%x th byte.\n", ((unsigned int)j) * FTS_PACKET_LENGTH);
        }
    }

    if ((dw_lenth) % FTS_PACKET_LENGTH > 0)
    {
        temp = packet_number * FTS_PACKET_LENGTH;
        packet_buf[2] = (u8)(temp>>8);
        packet_buf[3] = (u8)temp;

        temp = (dw_lenth) % FTS_PACKET_LENGTH;
        packet_buf[4] = (u8)(temp>>8);
        packet_buf[5] = (u8)temp;

        for (i=0;i<temp;i++)
        {
            packet_buf[6+i] = pbt_buf[ packet_number*FTS_PACKET_LENGTH + i]; 
            bt_ecc ^= packet_buf[6+i];
        }

        byte_write(&packet_buf[0],temp+6);    
        mdelay(20);
    }

    /***********send the last six byte**********/
    for (i = 0; i<6; i++)
    {
        temp = 0x6ffa + i;
        packet_buf[2] = (u8)(temp>>8);
        packet_buf[3] = (u8)temp;
        temp =1;
        packet_buf[4] = (u8)(temp>>8);
        packet_buf[5] = (u8)temp;
        packet_buf[6] = pbt_buf[ dw_lenth + i]; 
        bt_ecc ^= packet_buf[6];

        byte_write(&packet_buf[0],7);  
        mdelay(20);
    }

    /********send the opration head************/
    cmd_write(0xcc,0x00,0x00,0x00,1);
    byte_read(reg_val,1);
    printk("[TSP] Step 6:  ecc read 0x%x, new firmware 0x%x. \n", reg_val[0], bt_ecc);
    if(reg_val[0] != bt_ecc)
    {
        return ERR_ECC;
    }

    /*******Step 7: reset the new FW**********/
    cmd_write(0x07,0x00,0x00,0x00,1);
	mdelay(100);//100ms	
	fts_register_read(0xfc, buffer, 1);	
	if (buffer[0] == 1)
	{
	cmd=4;
	fts_register_write(0xfc, &cmd);
	mdelay(2500);//2500ms	
	 do	
	 {	
	 fts_register_read(0xfc, buffer, 1);	
	 mdelay(100);//100ms	
	 }while (buffer[0] != 1); 		   	
	}
    return ERR_OK;
}


/***********************************************************************/
void delay_qt_ms(unsigned long  w_ms)
{
    unsigned long i;
    unsigned long j;

    for (i = 0; i < w_ms; i++)
    {
        for (j = 0; j < 1000; j++)
        {
            udelay(1);
        }
    }
}

static int ft5x0x_i2c_rxdata(char *rxdata, int length)
{
	int ret;

	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= rxdata,
			.scl_rate = FT5406_IIC_SPEED,
		},
		{
			.addr	= this_client->addr,
			.flags	= I2C_M_RD,
			.len	= length,
			.buf	= rxdata,
			.scl_rate = FT5406_IIC_SPEED,
		},
	};

    //msleep(1);
	ret = i2c_transfer(this_client->adapter, msgs, 2);
	if (ret < 0)
		pr_err("msg %s i2c read error: %d\n", __func__, ret);
	
	return ret;
}

int ft5x02_i2c_Read(struct i2c_client *client,  char * writebuf, int writelen, char *readbuf, int readlen)
{
	int ret;

	if(writelen > 0)
	{
		struct i2c_msg msgs[] = {
			{
				.addr	= client->addr,
				.flags	= 0,
				.len	= writelen,
				.buf	= writebuf,
				.scl_rate = FT5406_IIC_SPEED,
			},
			{
				.addr	= client->addr,
				.flags	= I2C_M_RD,
				.len	= readlen,
				.buf	= readbuf,
				.scl_rate = FT5406_IIC_SPEED,
			},
		};
		ret = i2c_transfer(client->adapter, msgs, 2);
		if (ret < 0)
			pr_err("function:%s. i2c read error: %d\n", __func__, ret);
	}
	else
	{
		struct i2c_msg msgs[] = {
			{
				.addr	= client->addr,
				.flags	= I2C_M_RD,
				.len	= readlen,
				.buf	= readbuf,
				.scl_rate = FT5406_IIC_SPEED,
			},
		};
		ret = i2c_transfer(client->adapter, msgs, 1);
		if (ret < 0)
			pr_err("function:%s. i2c read error: %d\n", __func__, ret);
	}
	return ret;
}
/***********************************************************************************************
Name	:	 

Input	:	
                     

Output	:	

function	:	

***********************************************************************************************/
static int ft5x0x_i2c_txdata(char *txdata, int length)
{
	int ret;

	struct i2c_msg msg[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= length,
			.buf	= txdata,
			.scl_rate = FT5406_IIC_SPEED,
		},
	};

   	//msleep(1);
	ret = i2c_transfer(this_client->adapter, msg, 1);
	if (ret < 0)
		pr_err("%s i2c write error: %d\n", __func__, ret);

	return ret;
}

int ft5x02_i2c_Write(struct i2c_client *client, char *txdata, int length)
{
	int ret;

	struct i2c_msg msg[] = {
		{
			.addr	= client->addr,
			.flags	= 0,
			.len	= length,
			.buf	= txdata,
			.scl_rate = FT5406_IIC_SPEED,
		},
	};

   	//msleep(1);
	ret = i2c_transfer(client->adapter, msg, 1);
	if (ret < 0)
		pr_err("%s i2c write error: %d\n", __func__, ret);

	return ret;
}
/***********************************************************************************************
Name	:	 ft5x0x_write_reg

Input	:	addr -- address
                     para -- parameter

Output	:	

function	:	write register of ft5x0x

***********************************************************************************************/
static int ft5x0x_write_reg(u8 addr, u8 para)
{
    u8 buf[3];
    int ret = -1;

    buf[0] = addr;
    buf[1] = para;
    ret = ft5x0x_i2c_txdata(buf, 2);
    if (ret < 0) {
        pr_err("write reg failed! %#x ret: %d", buf[0], ret);
        return -1;
    }
    
    return 0;
}


int ft5x02_write_reg(struct i2c_client * client, u8 regaddr, u8 regvalue)
{
	unsigned char buf[2] = {0};
	buf[0] = regaddr;
	buf[1] = regvalue;

	return ft5x02_i2c_Write(client, buf, sizeof(buf));
}


/***********************************************************************************************
Name	:	ft5x0x_read_reg 

Input	:	addr
                     pdata

Output	:	

function	:	read register of ft5x0x

***********************************************************************************************/
static int ft5x0x_read_reg(u8 addr, u8 *pdata)
{
	int ret;
	u8 buf[2];
	struct i2c_msg msgs[2];

    //
	buf[0] = addr;    //register address
	
	msgs[0].addr = this_client->addr;
	msgs[0].flags = 0;
	msgs[0].len = 1;
	msgs[0].buf = buf;
	msgs[1].addr = this_client->addr;
	msgs[1].flags = I2C_M_RD;
	msgs[1].len = 1;
	msgs[1].buf = buf;

	ret = i2c_transfer(this_client->adapter, msgs, 2);
	if (ret < 0)
		pr_err("msg %s i2c read error: %d\n", __func__, ret);

	*pdata = buf[0];
	return ret;
  
}

int ft5x02_read_reg(struct i2c_client * client, u8 regaddr, u8 * regvalue)
{
	return ft5x02_i2c_Read(client, &regaddr, 1, regvalue, 1);
}


int fts_ctpm_auto_clb(void)
{
    unsigned char uc_temp;
    unsigned char i ;

    printk("[FTS] start auto CLB.\n");
    msleep(200);
    ft5x0x_write_reg(0, 0x40);  
    delay_qt_ms(100);   //make sure already enter factory mode
    ft5x0x_write_reg(2, 0x4);  //write command to start calibration
    delay_qt_ms(300);
    for(i=0;i<100;i++)
    {
        ft5x0x_read_reg(0,&uc_temp);
        if ( ((uc_temp&0x70)>>4) == 0x0)  //return to normal mode, calibration finish
        {
            break;
        }
        delay_qt_ms(200);
        printk("[FTS] waiting calibration %d\n",i);
        
    }
    printk("[FTS] calibration OK.\n");
    
    msleep(300);
    ft5x0x_write_reg(0, 0x40);  //goto factory mode
    delay_qt_ms(100);   //make sure already enter factory mode
    ft5x0x_write_reg(2, 0x5);  //store CLB result
    delay_qt_ms(300);
    ft5x0x_write_reg(0, 0x0); //return to normal mode 
    msleep(300);
    printk("[FTS] store CLB result OK.\n");
    return 0;
}


static int fts_ctpm_fw_upgrade_with_i_file(void)
{
   u8*     pbt_buf = 0;
   int i_ret;
    
   pbt_buf = CTPM_FW;
   i_ret =  fts_ctpm_fw_upgrade(pbt_buf,sizeof(CTPM_FW));
   //¼ÓÐ£×¼
 
   if (i_ret != 0)
   {
       printk("[FTS] upgrade failed i_ret = %d.\n", i_ret);
       //error handling ...
       //TBD
   }
   else
   {
       printk("[FTS] upgrade successfully.\n");
       fts_ctpm_auto_clb();  //start auto CLB
       fts_ctpm_auto_clb();  //start auto CLB
   }
   
   return i_ret;
}

/***********************************************************************/

unsigned char fts_ctpm_get_upg_ver(void)
{
    unsigned int ui_sz;
	
    ui_sz = sizeof(CTPM_FW);
    if (ui_sz > 2)
    {
        return CTPM_FW[ui_sz - 2];
    }
    else
        return 0xff; 
 
}

/*read the it7260 register ,used i2c bus*/
static int ft5406_read_regs(struct i2c_client *client, u8 reg, u8 buf[], unsigned len)
{
	int ret; 
	ret = i2c_master_reg8_recv(client, reg, buf, len, FT5406_IIC_SPEED);
	return ret; 
}


/* set the it7260 registe,used i2c bus*/
static int ft5406_set_regs(struct i2c_client *client, u8 reg, u8 const buf[], unsigned short len)
{
	int ret; 
	ret = i2c_master_reg8_send(client, reg, buf, (int)len, FT5406_IIC_SPEED);
	return ret;
}


static unsigned long nodata;
static int tp_no_touch;
#define	TIMER_MS_COUNTS		 				80 	//定时器的长度ms
static void touchpanel_check_timer(long unsigned _data)
{
	u8 i;
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	//printk("thiz_data============== %d\n",thiz_data);	
	if(tp_no_touch)
	{
		disable_irq_nosync(data->irq);
		for ( i=0;i<MAX_POINT;i++)   //清掉所有手指
		{
			input_mt_slot(data->input_dev,i);
			input_report_key(data->input_dev, BTN_TOUCH, 0);
			input_mt_report_slot_state(data->input_dev, MT_TOOL_FINGER, false);
		}		
		input_sync(data->input_dev);
		enable_irq(data->irq);	
		tp_no_touch =0;	
	}
	
	//mod_timer(&tp_timer,jiffies + msecs_to_jiffies(TIMER_MS_COUNTS));	
}

static u8 last_point_number;
static void zpp_read_touch_point( void )
{
	u8 point_Register=0x0;
	u8 point_buffer[40] = {0};
	u8 i=0,offset;
	
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);	
	int ret = ft5406_read_regs(data->client,point_Register, point_buffer, 6*MAX_POINT+1);
	if(ret < 0)
	{
		dev_err(&data->client->dev, "read touchpoint fail:%d!\n",ret);
		enable_irq(data->irq);
		return;		
	}
	
	//memset((data->event), 0, sizeof(data->event));
	data->event.touch_point =point_buffer[2] & 0x07;
	
	while(1)
	{
		offset = i*6+3;
		data->event.ts_objects[i].id = (s16)(point_buffer[offset+2] & 0xF0)>>4;
		data->event.ts_objects[i].status =(u8)((point_buffer[offset+0] & 0xC0) >> 6);		
		data->event.ts_objects[i].x = (((s16)(point_buffer[offset+0] & 0x0F))<<8) | ((s16)point_buffer[offset+1]);
		data->event.ts_objects[i].y = (((s16)(point_buffer[offset+2] & 0x0F))<<8) | ((s16)point_buffer[offset+3]);
		data->event.ts_objects[i].pressure = 200;	
		
		printk("zpp->Traced:num=%d,ID[%d]=%d----status[%d]=%x----x[%d]=%d-----y[%d]=%d\n",data->event.touch_point,i,data->event.ts_objects[i].id,i,data->event.ts_objects[i].status,i,data->event.ts_objects[i].x,i,data->event.ts_objects[i].y);
		if(i >=(last_point_number-1)) break;//record all point status in one frame data
		i++;
	}
	printk("---------------------------------------------------------------------------\n\n\n");
}

static void zpp_handle_touch_point_position( void )
{
	u8 i=0;
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	while(1)
	{
#if defined(CONFIG_LCD_SC_N81S)
		if(data->event.ts_objects[i].y > 742)
			data->event.ts_objects[i].y = 742; 
		if(data->event.ts_objects[i].y < 26)
			data->event.ts_objects[i].y = 26;		
#elif defined(CONFIG_LCD_SC_N71S) || defined(CONFIG_LCD_SC_N71SI)
		swap(data->event.ts_objects[i].x,data->event.ts_objects[i].y);
		//data->event.ts_objects[i].x = CONFIG_FOCALTECHTOUCH_MAX_X - data->event.ts_objects[i].x;
		data->event.ts_objects[i].y = CONFIG_FOCALTECHTOUCH_MAX_Y - data->event.ts_objects[i].y;
#elif defined(CONFIG_LCD_FLIP_VERTICAL)
		data->event.ts_objects[i].x = CONFIG_FOCALTECHTOUCH_MAX_X - data->event.ts_objects[i].x;
		data->event.ts_objects[i].y = CONFIG_FOCALTECHTOUCH_MAX_Y - data->event.ts_objects[i].y;
#endif	
	
		//printk("zpp->Traced 111:num=%d,ID[%d]=%d----status[%d]=%x----x[%d]=%d-----y[%d]=%d\n",data->event.touch_point,i,data->event.ts_objects[i].id,i,data->event.ts_objects[i].status,i,data->event.ts_objects[i].x,i,data->event.ts_objects[i].y);
		if(i >=(last_point_number-1)) break;
		i++;
	}	
}

static void zpp_report_touch_point( void )
{
	u8 i=0;
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	while(1)
	{
		if (data->event.ts_objects[i].status == 1) 
		{
			input_mt_slot(data->input_dev, data->event.ts_objects[i].id);
			input_report_key(data->input_dev, BTN_TOUCH, false);
			input_mt_report_slot_state(data->input_dev, MT_TOOL_FINGER, false);
		}  else if (data->event.ts_objects[i].status == 0 || data->event.ts_objects[i].status == 2) {
			input_mt_slot(data->input_dev, data->event.ts_objects[i].id);
			input_mt_report_slot_state(data->input_dev, MT_TOOL_FINGER, true);
			input_report_key(data->input_dev, BTN_TOUCH, true);
			input_report_abs(data->input_dev, ABS_MT_TRACKING_ID, data->event.ts_objects[i].id);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, data->event.ts_objects[i].x);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, data->event.ts_objects[i].y);
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, data->event.ts_objects[i].pressure);	
		}		
		//printk("zpp->Traced2222:num=%d,ID[%d]=%d----status[%d]=%x----x[%d]=%d-----y[%d]=%d\n",data->event.touch_point,i,data->event.ts_objects[i].id,i,data->event.ts_objects[i].status,i,data->event.ts_objects[i].x,i,data->event.ts_objects[i].y);
		if(i >=(last_point_number-1)) break;
		i++;
	}
	
	input_sync(data->input_dev);
	enable_irq(data->irq);		
}

static char last_num =0 ;
static uint8_t last_track_id[MAX_POINT] = {0};
#if 0
static void ft5406_queue_work(struct work_struct *work)
{
	struct ft5x0x_ts_data *data =  i2c_get_clientdata(this_client);//container_of(work, struct ft5x0x_ts_data, pen_event_work);
	
	
	zpp_read_touch_point();
	
	zpp_handle_touch_point_position();
	
	zpp_report_touch_point();
	
	tp_no_touch = 1;
	mod_timer(&tp_timer,jiffies + msecs_to_jiffies(TIMER_MS_COUNTS));
	last_point_number = data->event.touch_point;
	return;
}
#else
static void ft5406_queue_work(struct work_struct *work)
{
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);//container_of(work, struct ft5x0x_ts_data, pen_event_work);
	struct tp_event event;
	u8 start_reg=0x0;
	u8 buf[40] = {0};
	int ret,i=0,offset,points,j=0,temp = 0;
	u8 buf_w[1],buf_r[1];
	uint8_t track_id[MAX_POINT] = {0};		
		
#if CONFIG_FT5X0X_MULTITOUCH
	ret = ft5406_read_regs(data->client,start_reg, buf, 6*MAX_POINT+1);
#else
	ret = ft5406_read_regs(data->client,start_reg, buf, 7);
#endif
	if (ret < 0) {
		dev_err(&data->client->dev, "ft5406_read_regs fail:%d!\n",ret);
		enable_irq(data->irq);
		return;
	}
	
	points = buf[2] & 0x07;
	//dev_info(&data->client->dev, "ft5406_read_and_report_data points = %d\n",points);
	if (points == 0) {
NO_FINGER_REPORT:
#if   CONFIG_FT5X0X_MULTITOUCH
		for (i=0;i<last_num;i++)
		{
			input_mt_slot(data->input_dev,last_track_id[i]);
			input_report_key(data->input_dev, BTN_TOUCH, 0);
			//printk("different there go away  %d\n\n",last_track_id[i]);
			input_mt_report_slot_state(data->input_dev, MT_TOOL_FINGER, false);
		}
#else
		input_report_abs(data->input_dev, ABS_PRESSURE, 0);
		input_report_key(data->input_dev, BTN_TOUCH, 0);
#endif
		input_sync(data->input_dev);
		enable_irq(data->irq);

		return; 
	}
	memset(&event, 0, sizeof(struct tp_event));
#if CONFIG_FT5X0X_MULTITOUCH
	for(i=0;i<points;i++){
		offset = i*6+3;
		event.x = (((s16)(buf[offset+0] & 0x0F))<<8) | ((s16)buf[offset+1]);
		event.y = (((s16)(buf[offset+2] & 0x0F))<<8) | ((s16)buf[offset+3]);
		
#if defined(CONFIG_LCD_SC_N81S)
		if(event.y > 742)
			event.y = 742; 
		if(event.y < 26)
			event.y = 26;
#elif defined(CONFIG_LCD_SC_N91) 
		if (gpio_get_value (RK30_PIN6_PA5) == GPIO_LOW){
			buf_w[0] = 0x25;
			ft5406_set_regs(this_client,0x80,buf_w,1);
			//buf_r[0] = 0;
			//ft5406_read_regs(this_client,0x80,buf_r,1);
			//printk("----charged------------buf_r= %d\n",buf_r[0]);
		}
		else
		{
			buf_w[0] = 0x19;
			ft5406_set_regs(this_client,0x80,buf_w,1);		
			//ft5406_read_regs(this_client,0x80,buf_r,1);
			//printk("----no charged------------buf_r= %d\n",buf_r[0]);				
		}
			
#elif defined(CONFIG_LCD_SC_N71S) || defined(CONFIG_LCD_SC_N71SI)
	//	temp = event.x;
	//	event.x=event.y;
	//	event.y= temp;
		
	//	event.x = CONFIG_FOCALTECHTOUCH_MAX_X - event.x;
		event.y = CONFIG_FOCALTECHTOUCH_MAX_Y - event.y;
		
#endif

#if defined(CONFIG_LCD_FLIP_VERTICAL) || defined(CONFIG_LCD_SC_N71SI)
		event.x = CONFIG_FOCALTECHTOUCH_MAX_X - event.x;
		event.y = CONFIG_FOCALTECHTOUCH_MAX_Y - event.y; 
#endif
		
		event.id = (s16)(buf[offset+2] & 0xF0)>>4;
		event.flag = ((buf[offset+0] & 0xc0) >> 6);
		event.pressure = 200;
		track_id[i] = event.id;
		
//		if(( event.y >590 ) || ( event.x >1020) || (event.x<4) || (event.y<5))
//		{
//			goto NO_FINGER_REPORT;
//		}
		input_mt_slot(data->input_dev, event.id);
		input_mt_report_slot_state(data->input_dev, MT_TOOL_FINGER, true);
		input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, 200);		
		input_report_abs(data->input_dev, ABS_MT_POSITION_X,  event.x);
		input_report_abs(data->input_dev, ABS_MT_POSITION_Y,  event.y);
		//	input_report_abs(data->input_dev, ABS_MT_TRACKING_ID, event.id);
		//	input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
		//	input_mt_sync(data->input_dev);
		//printk("zpp--->traced:event.id=%d----x=%d----y=%d------event.flag=%d\n",event.id,event.x,event.y,event.flag);
	}
	if (points < last_num) //如果手指减少就去掉减少的手指，如果手指增加
	{
			for (i=0; i<last_num; i++)
			{
				if (last_track_id[i] != track_id[j])
				{
					input_mt_slot(data->input_dev,last_track_id[i]);
		//			printk("there go away  %d\n\n",last_track_id[i]);
					input_mt_report_slot_state(data->input_dev, MT_TOOL_FINGER, false);
				}
				else
				{
					j++;					
				}
			}
	}
	memcpy(last_track_id,track_id,MAX_POINT);
	tp_no_touch = 1;
	mod_timer(&tp_timer,jiffies + msecs_to_jiffies(TIMER_MS_COUNTS));

#endif
	input_sync(data->input_dev);
	last_num = points;
	enable_irq(data->irq);
	return;
}
#endif


static irqreturn_t ft5406_interrupt(int irq, void *dev_id)
{
	struct ft5x0x_ts_data *ft5x0x_ts = dev_id;

	disable_irq_nosync(ft5x0x_ts->irq);
//	if (!work_pending(&ft5x0x_ts->pen_event_work)) 
		queue_work(ft5x0x_ts->ts_workqueue, &ft5x0x_ts->pen_event_work);
	return IRQ_HANDLED;
}

static int ft5406_suspend(struct i2c_client *client, pm_message_t mesg)
{
	struct ft5x0x_ts_data *ft5x0x_ts = i2c_get_clientdata(client);
	struct focaltech_platform_data *pdata = client->dev.platform_data;
	u8 buf_w[1];
	int ret;
	
	buf_w[0] = 0x03;
	ft5406_set_regs(client,0xA5,buf_w,1);
	
	tp_no_touch = 1;
	if(ft5x0x_ts->irq)
		disable_irq_nosync(ft5x0x_ts->irq);
	ret = cancel_work_sync(&ft5x0x_ts->pen_event_work);
	if (ret && ft5x0x_ts->irq) /* if work was pending disable-count is now 2 */
		enable_irq(ft5x0x_ts->irq);
	mod_timer(&tp_timer,jiffies + msecs_to_jiffies(TIMER_MS_COUNTS));
	if (pdata->platform_sleep)                              
	pdata->platform_sleep();
	return 0;
}


static int ft5406_resume(struct i2c_client *client)
{
	struct ft5x0x_ts_data *ft5x0x_ts = i2c_get_clientdata(client);
	struct focaltech_platform_data *pdata = client->dev.platform_data;
	
	unsigned char reg_version;	
	u8 buf_r[1];
	
	enable_irq(ft5x0x_ts->irq);
	
	
	if (pdata->platform_sleep)                              
		pdata->platform_sleep();	
		
	msleep(20);
	
	if (pdata->platform_wakeup)                              
		pdata->platform_wakeup();
		
	msleep(20);
	
	tp_no_touch = 1;
	mod_timer(&tp_timer,jiffies + msecs_to_jiffies(TIMER_MS_COUNTS));	

#if 0
	fts_register_read(0xA3, &reg_version,1);
	//printk("%s--[TSP] IC version = 0x%x\n",__FUNCTION__, reg_version);	
	if( reg_version == 0x02)
	{	
		printk("%s--[TSP] IC version = 0x02---real\n",__FUNCTION__);		
		ft5x02_Init_IC_Param(client);
	}
#endif	
	//msleep(20);	
	//ft5x02_get_ic_param(client);
//	
//	buf_r[0] = 0;
//	ft5406_read_regs(client,0x88,buf_r,1);
//	printk("----------------buf_r= %d\n",buf_r[0]);
	
	return 0;
}

static void ft5406_suspend_early(struct early_suspend *h)
{
	ft5406_suspend(this_client,PMSG_SUSPEND);
}

static void ft5406_resume_early(struct early_suspend *h)
{
	ft5406_resume(this_client);
}
static int __devexit ft5406_remove(struct i2c_client *client)
{
	struct ft5x0x_ts_data *ft5x0x_ts = i2c_get_clientdata(client);

	free_irq(ft5x0x_ts->irq, ft5x0x_ts);
	input_unregister_device(ft5x0x_ts->input_dev);
	kfree(ft5x0x_ts);
	cancel_work_sync(&ft5x0x_ts->pen_event_work);
	destroy_workqueue(ft5x0x_ts->ts_workqueue);
	i2c_set_clientdata(client, NULL);
    unregister_early_suspend(&ft5406_power);
    this_client = NULL;
	return 0;
}



static int  ft5406_probe(struct i2c_client *client ,const struct i2c_device_id *id)
{
	struct ft5x0x_ts_data *ft5x0x_ts;
	struct input_dev *input_dev;
	struct focaltech_platform_data *pdata = client->dev.platform_data;
	int err = 0;
	int ret = 0;
	int retry = 0;
	u8 buf_w[1];
	u8 buf_r[1];
	const u8 buf_test[1] = {0};
    unsigned char reg_value;
    unsigned char reg_version;

	DBG_Trace("TouchPanel-->%s--Ready",__func__);

	if (!pdata) {
		dev_err(&client->dev, "platform data is required!\n");
		return -EINVAL;
	}

	if (pdata->init_platform_hw)                              
		pdata->init_platform_hw();

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)){
		dev_err(&client->dev, "Must have I2C_FUNC_I2C.\n");
		return -ENODEV;
	}
	
	ft5x0x_ts = kzalloc(sizeof(*ft5x0x_ts), GFP_KERNEL);
	if (!ft5x0x_ts)	{
		return -ENOMEM;
	}

	while(retry < 5)
	{
		ret=ft5406_read_regs(client,FT5X0X_REG_PMODE, buf_test,1);
		if(ret > 0)break;
		retry++;
	}
	if(ret <= 0)
	{
		printk("FT5406 I2C TEST ERROR!\n");
		err = -ENODEV;
		goto exit_i2c_test_fail;
	}
	
	input_dev = input_allocate_device();
	if (!input_dev) {
		err = -ENOMEM;
		printk("failed to allocate input device\n");
		goto exit_input_dev_alloc_failed;
	}
	ft5x0x_ts->client = this_client = client;
	ft5x0x_ts->irq = client->irq;
	ft5x0x_ts->input_dev = input_dev;

  #if   CONFIG_FT5X0X_MULTITOUCH
  	__set_bit(INPUT_PROP_DIRECT, input_dev->propbit);
	__set_bit(EV_ABS,input_dev->evbit);

	input_mt_init_slots(input_dev, MAX_POINT);
#if 0	
	set_bit(ABS_MT_POSITION_X, input_dev->absbit);
	set_bit(ABS_MT_POSITION_Y, input_dev->absbit);
	set_bit(ABS_MT_TOUCH_MAJOR, input_dev->absbit);
	set_bit(ABS_MT_TRACKING_ID, input_dev->absbit);
	set_bit(ABS_MT_WIDTH_MAJOR, input_dev->absbit);
#endif
	input_set_abs_params(input_dev,ABS_MT_POSITION_X, 0, SCREEN_MAX_X, 0, 0);
	input_set_abs_params(input_dev,ABS_MT_POSITION_Y, 0, SCREEN_MAX_Y, 0, 0);
	input_set_abs_params(input_dev,ABS_MT_TOUCH_MAJOR, 0, TOUCH_MAJOR_MAX, 0, 0);
	//input_set_abs_params(input_dev,ABS_MT_TRACKING_ID, 0, MAX_POINT, 0, 0);
	//input_set_abs_params(input_dev,ABS_MT_WIDTH_MAJOR, 0, WIDTH_MAJOR_MAX, 0, 0);
#else
	set_bit(ABS_X, input_dev->absbit);
	set_bit(ABS_Y, input_dev->absbit);
	set_bit(ABS_PRESSURE, input_dev->absbit);
	set_bit(BTN_TOUCH, input_dev->keybit);
	input_set_abs_params(input_dev, ABS_X, 0, SCREEN_MAX_X, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, SCREEN_MAX_Y, 0, 0);
	input_set_abs_params(input_dev, ABS_PRESSURE, 0, PRESS_MAX, 0 , 0);
#endif


	set_bit(EV_ABS, input_dev->evbit);
	set_bit(EV_KEY, input_dev->evbit);
	
	input_dev->name		= FT5X0X_NAME;		//dev_name(&client->dev)
	err = input_register_device(input_dev);
	if (err) {
		printk("ft5x0x_ts_probe: failed to register input device: \n");
		goto exit_input_register_device_failed;
	}

	if (!ft5x0x_ts->irq) {
		err = -ENODEV;
		dev_err(&ft5x0x_ts->client->dev, "no IRQ?\n");
		goto exit_no_irq_fail;
	}else{
		ft5x0x_ts->irq = gpio_to_irq(ft5x0x_ts->irq);
	}

	//INIT_WORK(&ft5x0x_ts->pen_event_work, ft5406_ts_pen_irq_work);
	INIT_WORK(&ft5x0x_ts->pen_event_work, ft5406_queue_work);
	ft5x0x_ts->ts_workqueue = create_singlethread_workqueue("ft5x0x_ts");
	setup_timer(&tp_timer,touchpanel_check_timer, (unsigned long)nodata);
	
	if (!ft5x0x_ts->ts_workqueue) {
		err = -ESRCH;
		goto exit_create_singlethread;
	}

	/***wait CTP to bootup normally***/
	 msleep(200); 
#if 0
	 fts_register_read(0xA3, &reg_version,1);
	 printk("[TSP] IC version = 0x%x\n", reg_version);	 
	//ft5x02_get_ic_param(client);
	if( reg_version == 0x02)
	{
		printk("[TSP] IC version --- ft5x02 \n ");	
		ft5x02_Init_IC_Param(this_client);
	}
#endif	
	 
//just for update,Éý¼¶Íê£¬ÐèÒªÐ£×¼¡£
#if 0//defined(CONFIG_LCD_SC_N81S)

	 if((sizeof(CTPM_FW)/sizeof(CTPM_FW[0])) <= 2)
	 {
	 	goto focaltech_on_fw_update;
	 }
 
	 fts_register_read(FT5X0X_REG_FIRMID, &reg_version,1);
	 printk("[TSP] firmware version = 0x%x\n", reg_version);
	 fts_register_read(FT5X0X_REG_REPORT_RATE, &reg_value,1);
	 //max rate == 13,  Êµ¼ÊÆµÂÊÎªreg_value*10
	 // Èí¼þÒ²¿ÉÒÔÍ¨¹ýÐ´¸Ã¼Ä´æÆ÷À´¸Ä±ärate
	 printk("[TSP]firmware report rate = %dHz\n", reg_value*10);
	 fts_register_read(FT5X0X_REG_THRES, &reg_value,1);
	 printk("[TSP]firmware threshold = %d\n", reg_value * 4);
	 fts_register_read(FT5X0X_REG_NOISE_MODE, &reg_value,1);
	 printk("[TSP]nosie mode = 0x%2x\n", reg_value);
 
	  if (fts_ctpm_get_upg_ver() != reg_version)  
	  {
		  printk("[TSP] start upgrade new verison 0x%2x\n", fts_ctpm_get_upg_ver());
		  msleep(200);
		  err =  fts_ctpm_fw_upgrade_with_i_file();
		  if (err == 0)
		  {
			  printk("[TSP] ugrade successfuly.\n");
			  msleep(300);
			  fts_register_read(FT5X0X_REG_FIRMID, &reg_value,1);
			  printk("FTS_DBG from old version 0x%2x to new version = 0x%2x\n", reg_version, reg_value);
		  }
		  else
		  {
			  printk("[TSP]  ugrade fail err=%d, line = %d.\n",err, __LINE__);
		  }
		  msleep(4000);
	  }
	  if (reg_value != 8)
	  {
		  ft5x0x_write_reg(FT5X0X_REG_REPORT_RATE, 10);  
		  delay_qt_ms(100);   //make sure already enter factory mode
		  fts_register_read(FT5X0X_REG_REPORT_RATE, &reg_value,1);
		  //max rate == 13,  Êµ¼ÊÆµÂÊÎªreg_value*10
		  // Èí¼þÒ²¿ÉÒÔÍ¨¹ýÐ´¸Ã¼Ä´æÆ÷À´¸Ä±ärate
		  printk("[TSP]firmware report rate = %dHz now \n", reg_value*10);


	  }
focaltech_on_fw_update:
#endif
	//printk("client->dev.driver->name %s  ,%d \n",client->dev.driver->name,ft5x0x_ts->irq);
	ret = request_irq(ft5x0x_ts->irq, ft5406_interrupt, IRQF_TRIGGER_FALLING, client->dev.driver->name, ft5x0x_ts);
	if (ret < 0) {
		dev_err(&client->dev, "irq %d busy?\n", ft5x0x_ts->irq);
		goto exit_irq_request_fail;
	}
	
	i2c_set_clientdata(client, ft5x0x_ts);
	ft5406_power.suspend =ft5406_suspend_early;
	ft5406_power.resume =ft5406_resume_early;
	ft5406_power.level = EARLY_SUSPEND_LEVEL_DISABLE_FB+1;
	register_early_suspend(&ft5406_power);

//	buf_w[0] = 9;
//	err = ft5406_set_regs(client,0x88,buf_w,1);
//	buf_r[0] = 0;
//	err = ft5406_read_regs(client,0x88,buf_r,1);
//	printk("----------------buf_r= %d\n",buf_r[0]);

	DBG_Trace("TouchPanel-->%s--OK!!!",__func__);
    
	return 0;

	i2c_set_clientdata(client, NULL);
	free_irq(ft5x0x_ts->irq,ft5x0x_ts);
exit_irq_request_fail:
	cancel_work_sync(&ft5x0x_ts->pen_event_work);
	destroy_workqueue(ft5x0x_ts->ts_workqueue);
	del_timer_sync(&tp_timer);
exit_create_singlethread:
exit_no_irq_fail:
	input_unregister_device(input_dev);
exit_input_register_device_failed:
	input_free_device(input_dev);
exit_input_dev_alloc_failed:
exit_i2c_test_fail:
	
if (pdata->exit_platform_hw)                              
	pdata->exit_platform_hw();
	kfree(ft5x0x_ts);
	return err;
}

static int  ft5406_shutdown(struct i2c_client *client)
{
	int ret  = 0 ;
	printk("%s....................%s\n",__FUNCTION__,__FUNCTION__);

	ft5406_suspend(client,PMSG_SUSPEND);
	
}

static struct i2c_device_id ft5406_idtable[] = {
	{ FT5X0X_NAME, 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, ft5406_idtable);

static struct i2c_driver ft5406_driver  = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= FT5X0X_NAME
	},
	.id_table	= ft5406_idtable,
	.probe      = ft5406_probe,
	.remove 	= __devexit_p(ft5406_remove),
	.shutdown     =ft5406_shutdown,
};

static int __init ft5406_ts_init(void)
{
	return i2c_add_driver(&ft5406_driver);
}

static void __exit ft5406_ts_exit(void)
{
	printk("Touchscreen driver of ft5406 exited.\n");
	i2c_del_driver(&ft5406_driver);
	del_timer_sync(&tp_timer);
}


/***********************************************************************/

device_initcall_sync(ft5406_ts_init);
module_exit(ft5406_ts_exit);

MODULE_AUTHOR("<wenfs@Focaltech-systems.com>");
MODULE_DESCRIPTION("FocalTech ft5x0x TouchScreen driver");

