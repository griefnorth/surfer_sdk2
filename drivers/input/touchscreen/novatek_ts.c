
/***********************************************************************
*drivers/input/touchsrceen/novatek_touchdriver.c
*
*Novatek  nt1100x TouchScreen driver.
*
*Copyright(c) 2012 Novatek Ltd.
*
*VERSION              DATA               AUTHOR
*   1.0                   2012-02-24         LiuPeng
************************************************************************/


#include <linux/i2c.h>
#include <linux/input.h>
#include "novatek_ts.h"
#include <linux/earlysuspend.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/input/mt.h> 	//for Linux3.0
#include <linux/irq.h>
#include <asm/mach/irq.h>
#include <linux/device.h>
#include <linux/slab.h> //for Kzalloc
#include <mach/board.h>
/*******************************************************/
// Chip Reset define 
#define  HW_RST      0
#define  SW_RST      1

#if 0
#define KFprintk(x...) printk(x)
#else
#define KFprintk(x...) do{} while(0)
#endif
static struct early_suspend novatek_power; //for ealy suspend
static struct i2c_client *this_client;

#if defined(CONFIG_LCD_SC_S12)
#define _NOVATEK_CAPACITANCEPANEL_BOOTLOADER_FUNCTION_
#else

#endif

#ifdef _NOVATEK_CAPACITANCEPANEL_BOOTLOADER_FUNCTION_
enum
{
  RS_OK         = 0,
  RS_INIT_ER    = 8,
  RS_ERAS_ER    = 9,
  RS_FLCS_ER    = 10,
  RS_WD_ER      = 11
} ;
#endif

/*******************************************************	
Description:
	Read data from the i2c slave device;
	This operation consisted of 2 i2c_msgs,the first msg used
	to write the operate address,the second msg used to read data.

Parameter:
	client:	i2c device.
	buf[0]:operate address.
	buf[1]~buf[len]:read data buffer.
	len:operate length.
	
return:
	numbers of i2c_msgs to transfer
*********************************************************/
static int i2c_read_bytes(struct i2c_client *client, uint8_t *buf, int len)
{
	struct i2c_msg msgs[2];
	int ret=-1;
	int retries = 0;

	msgs[0].flags=client->flags;
	msgs[0].addr=client->addr;
	msgs[0].len=1;
	msgs[0].buf=&buf[0];
	msgs[0].scl_rate = NOVATEK_I2C_SCL;

	msgs[1].flags=client->flags | I2C_M_RD;
	msgs[1].addr=client->addr;
	msgs[1].len=len-1;
	msgs[1].buf=&buf[1];
	msgs[1].scl_rate = NOVATEK_I2C_SCL;

	while(retries<5)
	{
		ret=i2c_transfer(client->adapter,msgs, 2);
		if(ret == 2)break;
		retries++;
	}
	return ret;
}


/*******************************************************	
Description:
	write data to the i2c slave device.

Parameter:
	client:	i2c device.
	buf[0]:operate address.
	buf[1]~buf[len]:write data buffer.
	len:operate length.
	
return:
	numbers of i2c_msgs to transfer.
*********************************************************/
static int i2c_write_bytes(struct i2c_client *client,uint8_t *data,int len)
{
	struct i2c_msg msg;
	int ret=-1;
	int retries = 0;

	msg.flags=!I2C_M_RD;
	msg.addr=client->addr;
	msg.len=len;
	msg.buf=data;
	msg.scl_rate = NOVATEK_I2C_SCL;
	
	while(retries<5)
	{
		ret=i2c_transfer(client->adapter,&msg, 1);
		if(ret == 1)break;
		retries++;
	}
	return ret;
}
/*******************************************************
Description:
	novatek touchscreen initialize function.

Parameter:
	ts:	i2c client private struct.
	
return:
	Executive outcomes.0---succeed.
*******************************************************/
static int novatek_init_panel(struct novatek_ts_data *ts)
{
	ts->abs_x_max = SCREEN_MAX_WIDTH;
	ts->abs_y_max = SCREEN_MAX_HEIGHT;
	ts->max_touch_num = MAX_FINGER_NUM;
	ts->int_trigger_type = 2;	//edge_falling
	
	return 0;
}
/*******************************************************
Description:
	novatek touchscreen driver Get ChipID function.

Parameter:
	client:	i2c device struct.
	
return:
	Executive outcomes.  0x01  --NT11003 ChipID.
*******************************************************/
static int novatek_ts_ChipID(struct i2c_client *client)
{
	int i,ret;
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	uint8_t Read_data[7] = {0x00,};				
	
	for(i=0;i < 30; i++)
	{		
		msleep(10);
		ret =i2c_read_bytes(ts->client, Read_data, 5); 
		if (ret < 0)
			return ret;
		else
			return 1;
	}
}
/*******************************************************
Description:
	novatek touchscreen work function.

Parameter:
	ts:	i2c client private struct.
	
return:
	Executive outcomes.0---succeed.
*******************************************************/
static void novatek_ts_work_func(struct work_struct *work)
{
	int ret =-1;
	static u8 points_last_flag[MAX_FINGER_NUM]={0};
	uint8_t  point_data[1+ IIC_BYTENUM*MAX_FINGER_NUM]={0};
	uint8_t  index,touch_num,trackid;
	uint8_t  track_flag[MAX_FINGER_NUM];
	uint16_t position = 0;	
	struct tp_event event;
	struct tp_event  current_events[MAX_FINGER_NUM];
	int i = 0;
	
	struct novatek_ts_data *ts = container_of(work, struct novatek_ts_data,work);

	point_data[0] = READ_COOR_ADDR;
	ret = i2c_read_bytes(ts->client, point_data, sizeof(point_data)/sizeof(point_data[0]));
	
	if(ret < 0)
	{
		dev_info(&ts->client->dev, "Read point data failed !\n");
		enable_irq(ts->client->irq);
		return ;
	}
	
#if(IC_DEFINE == NT11003 || IC_DEFINE == NT11002)

//get touch number 	koffuxu
	touch_num = MAX_FINGER_NUM;
	for(index = 0; index < MAX_FINGER_NUM; index++)
	{
		position = 1 + IIC_BYTENUM * index;
		track_flag[index] = 0;
		if(( point_data[position] & 0x03 ) == 0x03 )		
		  //  touch_num--;
		    track_flag[index] = 0;
		else
			track_flag[index] = 1;
		
	}
	KFprintk("\n\n>>> koffuxu Touch_num = %d\n",touch_num);
/*
//report Up event
	if(touch_num == 0)		
	{
		for(index=0;index<MAX_FINGER_NUM;index++)
		{
			KFprintk(">>>koffuxu Up Event!\n");
			input_mt_slot(ts->input_dev,index);
			input_mt_report_slot_state(ts->input_dev, MT_TOOL_FINGER, false);
		}
		memset(points_last_flag, 0, sizeof(points_last_flag));
		input_sync(ts->input_dev);
		enable_irq(ts->client->irq);
		return; 
	}
*/
//Init struct
	memset(&event, 0, sizeof(struct tp_event));
	memset(current_events, 0, sizeof(current_events));

//report DN event		  
	//for(index = 0; index < touch_num; index++)
	for(index = 0; index < MAX_FINGER_NUM; index++)
	{
	  	if(track_flag[index] == 1)
	  	{
	    	position = 1 +IIC_BYTENUM * index;
			//Read the point_data data				
			event.x = (unsigned int)(point_data[position+1]<<4) + (unsigned int) (point_data[position+3]>>4);
			event.y= (unsigned int)(point_data[position+2]<<4) + (unsigned int) (point_data[position+3]&0x0f);
			event.id = (unsigned int)(point_data[position] >> 3) - 1;
			event.flag= (unsigned int)( point_data[position] & 0x03 );  //0x01 Down; 0x02 Move; 0x03 Up;
			event.pressure = 200;

			KFprintk(">>>koffuxu index[%d] event.id = %d###event.flag =%d \n",index,event.id,event.flag);
			//printk("\n>>>>>>Origan Value input_x = %d,input_y = %d, input_w = %d\n", input_x, input_y, input_w);
			//Scale point data
			//event.x = event.x *SCREEN_MAX_WIDTH/(TOUCH_MAX_WIDTH);	
			//event.y = event.y *SCREEN_MAX_HEIGHT/(TOUCH_MAX_HEIGHT);
		
			KFprintk(">>>>>>Scal Value input_x = %d,input_y = %d\n", event.x, event.y);
			if((event.x < SCREEN_MAX_WIDTH)||(event.y < SCREEN_MAX_HEIGHT))
			{
				if(event.flag /*== 0x01*/)
				memcpy(&current_events[event.id], &event, sizeof(event));	
			}

			if( current_events[index].flag/*== 0x02*/)
			{
				KFprintk(">>>Koffuxu Down Event\n");
				input_mt_slot(ts->input_dev, index);
				input_mt_report_slot_state(ts->input_dev, MT_TOOL_FINGER, true);
				input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR, 1);
				input_report_abs(ts->input_dev, ABS_MT_POSITION_X, current_events[index].x);
				input_report_abs(ts->input_dev, ABS_MT_POSITION_Y, current_events[index].y);
			}

			points_last_flag[index] = current_events[index].flag;
	  	}
	 	else
	  	{
	  		if(points_last_flag[index]){
				input_mt_slot(ts->input_dev,index);
				input_mt_report_slot_state(ts->input_dev, MT_TOOL_FINGER, false);
				points_last_flag[index] = 0;
			}
	  	}
		 
		
	}
			  
				
	input_sync(ts->input_dev);
	enable_irq(ts->client->irq);
	return;


//	goto END_WORK_FUNC;
/*	TouchKey_Handle:
		#ifdef TOUCHKEY_EVENT
		value = (unsigned int)(point_data[1] >> 3);
		switch(value)
			{
				case MENU:
			    	input_report_key(ts->input_dev, KEY_MENU, point_data[1]&0x01);
				break;
				case HOME:
			    	input_report_key(ts->input_dev, KEY_HOME, point_data[1]&0x01);
				break;			
				case BACK:
			    	input_report_key(ts->input_dev, KEY_BACK, point_data[1]&0x01);
				break;
				case VOLUMEDOWN:
			    	input_report_key(ts->input_dev, KEY_VOLUMEDOWN, point_data[1]&0x01);
				break;
				case VOLUMEUP:
			    	input_report_key(ts->input_dev, KEY_VOLUMEUP, point_data[1]&0x01);
				break;
				default:
				break;
			}
		input_sync(ts->input_dev);
		#endif*/
#endif

			
}
/*******************************************************
Description:
	Timer interrupt service routine.

Parameter:
	timer:	timer struct pointer.
	
return:
	Timer work mode. HRTIMER_NORESTART---not restart mode
*******************************************************/
static enum hrtimer_restart novatek_ts_timer_func(struct hrtimer *timer)
{
	struct novatek_ts_data *ts = container_of(timer, struct novatek_ts_data, timer);
	queue_work(novatek_wq, &ts->work);
	hrtimer_start(&ts->timer, ktime_set(0, (POLL_TIME+6)*1000000), HRTIMER_MODE_REL);
	return HRTIMER_NORESTART;
}
/*******************************************************
Description:
	External interrupt service routine.

Parameter:
	irq:	interrupt number.
	dev_id: private data pointer.
	
return:
	irq execute status.
*******************************************************/
static irqreturn_t novatek_ts_irq_handler(int irq, void *dev_id)
{
	struct novatek_ts_data *ts = dev_id;
	disable_irq_nosync(ts->client->irq);
	queue_work(novatek_wq, &ts->work);
//	printk(">>>koffuxu enter %s\n",__func__);
	return IRQ_HANDLED;
}
/*******************************************************
Description:
	novatek touchscreen probe function.

Parameter:
	client:	i2c device struct.
	id:device id.
	
return:
	Executive outcomes. 0---succeed.
*******************************************************/


static int novatek_suspend(struct i2c_client *client, pm_message_t mesg)
{
	int ret;
//	struct ft5x0x_ts_data *ft5x0x_ts = i2c_get_clientdata(client);
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	struct novatek_platform_data *pdata = client->dev.platform_data;
//	u8  buf[1] = {3};
//	ft5406_set_regs(client,0xa5,buf,1);
	//write TP code to suspend IC
/*	uint8_t serialinterface_data[] ={0xff, 0x8f, 0xff};
  	uint8_t suspend_data[]= {0x00, 0xAE};
	ret = i2c_write_bytes(ts->client, serialinterface_data, (sizeof(serialinterface_data)/sizeof(serialinterface_data[0])));
	if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
	ret = i2c_write_bytes(ts->client, suspend_data, (sizeof(suspend_data)/sizeof(suspend_data[0])));
    	if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
*/
	if (pdata->platform_sleep)                              
		pdata->platform_sleep();
	disable_irq(client->irq);
	return 0;
}


static int novatek_resume(struct i2c_client *client)
{
//	struct ft5x0x_ts_data *ft5x0x_ts = i2c_get_clientdata(client);
    u8 write_cmd1[3] = {0xff,0x3F,0xF0};
    u8 write_cmd2[6] = {0x00,0x48,0x54,0x11,0x00,0x3b};  //Chargpump Power on command
	
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	struct novatek_platform_data *pdata = client->dev.platform_data;
	
	enable_irq(client->irq);
	if (pdata->platform_wakeup)                              
		pdata->platform_wakeup();
		
#if 0		
	msleep(50);
	
	i2c_write_bytes(ts->client, write_cmd1, 3); 
	msleep(10);
	i2c_write_bytes(ts->client, write_cmd2, 6);	
#endif		
	return 0;
}

static void novatek_suspend_early(struct early_suspend *h)
{
	novatek_suspend(this_client,PMSG_SUSPEND);
}

static void novatek_resume_early(struct early_suspend *h)
{
	novatek_resume(this_client);
}

static int	nvctp_CheckIsBootloader(struct i2c_client *client);

static int novatek_ts_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int ret = 0;
	int chipid = 0; 
	struct novatek_ts_data *ts;
	const char irq_table[4] = {
								IRQ_TYPE_EDGE_RISING,
								IRQ_TYPE_EDGE_FALLING,
							   	IRQ_TYPE_LEVEL_LOW,
							   	IRQ_TYPE_LEVEL_HIGH};
								
	DBG_Trace("TouchPanel-->%s--Ready",__func__);
	
	//platform data Koffuxu
	struct novatek_platform_data *pdata = client->dev.platform_data;
	if (!pdata) {
		dev_err(&client->dev, "platform data is required!\n");
		return -EINVAL;
	}

	if (pdata->init_platform_hw)                              
		pdata->init_platform_hw();
	
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) 
	{		
		ret = -ENODEV;
		goto err_check_functionality_failed;
	}
	
	ts = kzalloc(sizeof(*ts), GFP_KERNEL);
	if (ts == NULL) {
		ret = -ENOMEM;
		goto err_alloc_data_failed;
	}
	
	ret = novatek_init_panel(ts);
	ts->client = this_client = client;
	dev_info(&client->dev, "platform data before i2c_set_clientdata!\n");
	i2c_set_clientdata(client, ts);
	
	//Read and Print ChipID and test IC;
	dev_info(&client->dev, "platform data before start read chipID !\n");
	chipid = novatek_ts_ChipID(ts->client);
	//chipid  = 1;
	if(chipid < 0)
	{	
		printk(">>>koffuxu Novatek IC test Error ret = %d\n",chipid);
		goto err_init_panel_fail;
	}
	else
		printk(">>>>>>>>>>>>>>>>>>novatek CHIP ID = %x\n",chipid);
	

	INIT_WORK(&ts->work, novatek_ts_work_func);
	
	ret  = request_irq(client->irq, novatek_ts_irq_handler ,  irq_table[ts->int_trigger_type],
			client->name, ts);

	if(ret != 0) {
			dev_err(&client->dev,"Novatek ts probe: request irq failed.\n");
			goto exit_irq_request_failed;
		}
	ts->input_dev = input_allocate_device();
	if (ts->input_dev == NULL) {
		ret = -ENOMEM;
		printk("Failed to allocate input device\n");
		goto err_input_dev_alloc_failed;
	}
	/*******************************************************************************/
//	ts->input_dev->evbit[0] = BIT_MASK(EV_SYN) | BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS);
//	ts->input_dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);
//	ts->input_dev->absbit[0] = BIT(ABS_X) | BIT(ABS_Y) | BIT(ABS_PRESSURE); 
	
    #if 0 //def TOUCHKEY_EVENT
		for(retry = 0; retry < MAX_KEY_NUM; retry++)
		{
			input_set_capability(ts->input_dev,EV_KEY,touch_key_array[retry]);	
		}
   #endif
  	__set_bit(INPUT_PROP_DIRECT,ts->input_dev->propbit);
	__set_bit(EV_ABS, ts->input_dev->evbit);	
	input_mt_init_slots(ts->input_dev, MAX_FINGER_NUM);
	input_set_abs_params(ts->input_dev, ABS_MT_TOUCH_MAJOR, 0, 255, 0, 0);
	input_set_abs_params(ts->input_dev, ABS_MT_POSITION_X, 0, ts->abs_x_max, 0, 0);
	input_set_abs_params(ts->input_dev, ABS_MT_POSITION_Y, 0, ts->abs_y_max, 0, 0);
	
	/*******************************************************************************/
	sprintf(ts->phys, "input/ts");
	ts->input_dev->name = novatek_ts_name;
	ts->input_dev->phys = ts->phys;
	ts->input_dev->id.bustype = BUS_I2C;
	ts->input_dev->id.vendor = 0xDEAD;
	ts->input_dev->id.product = 0xBEEF;
	ts->input_dev->id.version = 0224;	//screen firmware version

	ret = input_register_device(ts->input_dev);
	if (ret != 0) 
	{
		dev_err(&client->dev,
			"Probe: unable to register %s input device\n", ts->input_dev->name);
		goto err_input_register_device_failed;
	}
	/****************************************************************/
	// BootLoader Function...
	#ifdef _NOVATEK_CAPACITANCEPANEL_BOOTLOADER_FUNCTION_
	disable_irq(client->irq);
	nvctp_CheckIsBootloader(ts->client);
	enable_irq(client->irq);
	#endif
	/****************************************************************/
	if(ts->use_irq)
		enable_irq(client->irq);
//for power manager
	novatek_power.suspend =novatek_suspend_early;
	novatek_power.resume =novatek_resume_early;
	novatek_power.level = 0x2;
	register_early_suspend(&novatek_power);
	
	DBG_Trace("TouchPanel-->%s--OK!!!",__func__);
	return 0;  
	
	err_input_register_device_failed:
		input_free_device(ts->input_dev);
	err_input_dev_alloc_failed:
		free_irq(client->irq, ts);	
	exit_irq_request_failed:
		cancel_work_sync(&ts->work);
		//destroy_workqueue(ts->workqueue);
	//exit_singlethread:
	//	i2c_set_clientdata(client, NULL);
	//	kfree(ts);
	err_alloc_data_failed:	
	err_check_functionality_failed:
		printk( "Must have I2C_FUNC_I2C.\n");	
	err_init_panel_fail:
	if (pdata->exit_platform_hw)
		pdata->exit_platform_hw();
		kfree(ts);
	return ret;
		
}


/*******************************************************
Description:
	novatek touchscreen driver release function.

Parameter:
	client:	i2c device struct.
	
return:
	Executive outcomes. 0---succeed.
*******************************************************/
static int novatek_ts_remove(struct i2c_client *client)
{
	struct novatek_ts_data *ts = i2c_get_clientdata(client);

	if (ts && ts->use_irq) {
		free_irq(client->irq, ts);
	}
	else if(ts){
			hrtimer_cancel(&ts->timer);
		}
	dev_notice(&client->dev, "The driver is removing...\n");
	i2c_set_clientdata(client, NULL);
	input_unregister_device(ts->input_dev);
	unregister_early_suspend(&novatek_power);
	kfree(ts);
	return 0;
}

#if 0
static int novatek_ts_suspend(struct i2c_client *client, pm_message_t mesg)
{
	int ret;
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	uint8_t serialinterface_data[] ={0xff, 0x8f, 0xff};
    uint8_t suspend_data[]= {0x00, 0xAE};

	ret = i2c_write_bytes(ts->client, serialinterface_data, (sizeof(serialinterface_data)/sizeof(serialinterface_data[0])));
	if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
	ret = i2c_write_bytes(ts->client, suspend_data, (sizeof(suspend_data)/sizeof(suspend_data[0])));
    if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
	if (ts->use_irq)
		disable_irq(client->irq);
	else
		hrtimer_cancel(&ts->timer);
#if 0	//Koffuxu
	if (ts->power) {
		ret = ts->power(ts, 0);
		if (ret < 0)
			printk(KERN_ERR "nt11003_ts_resume power off failed\n");
	}
#endif
	return 0;
}
static int novatek_ts_resume(struct i2c_client *client)
{
	int ret;
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	uint8_t serialinterface_data[] ={0xff, 0x8f, 0xff};
    uint8_t suspend_data[]= {0x00, 0x00};
    ret = i2c_write_bytes(ts->client, serialinterface_data, (sizeof(serialinterface_data)/sizeof(serialinterface_data[0])));
    if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}

	ret = i2c_write_bytes(ts->client, suspend_data, (sizeof(suspend_data)/sizeof(suspend_data[0])));
    msleep(20);
	if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
#if 0	//Koffuxu    
	if (ts->power) {
		ret = ts->power(ts, 1);
		if (ret < 0)
			printk(KERN_ERR "novatek_ts_resume power on failed\n");
	}
#endif
	if (ts->use_irq)
		enable_irq(client->irq);
	else
		hrtimer_start(&ts->timer, ktime_set(1, 0), HRTIMER_MODE_REL);

	return 0;
}
#endif

#ifdef _NOVATEK_CAPACITANCEPANEL_BOOTLOADER_FUNCTION_

#define  FW_DATASIZE      (1024*32)
#define  FLASHSECTORSIZE  (FW_DATASIZE/128)
#define  FW_CHECKSUM_ADDR     (FW_DATASIZE - 8)

/*******************************************************	
Description:
	Read data from the flash slave device;
	This operation consisted of 2 i2c_msgs,the first msg used
	to write the operate address,the second msg used to read data.

Parameter:
	client:	i2c device.
	buf[0]:operate address.
	buf[1]~buf[len]:read data buffer.
	len:operate length.
	
return:
	numbers of i2c_msgs to transfer
*********************************************************/

static int BootLoader_read_bytes(struct i2c_client *client, uint8_t *buf, int len)
{
	struct i2c_msg msgs[2];
	int ret=-1;
	int retries = 0;

	msgs[0].flags=client->flags;
	msgs[0].addr=0x7F;//client->addr;
	msgs[0].len=1;
	msgs[0].buf=&buf[0];
	msgs[0].scl_rate = NOVATEK_I2C_SCL;
	
	msgs[1].flags=client->flags | I2C_M_RD;;
	msgs[1].addr=0x7F;//client->addr;
	msgs[1].len=len-1;
	msgs[1].buf=&buf[1];
	msgs[1].scl_rate = NOVATEK_I2C_SCL;
	while(retries<5)
	{
		ret=i2c_transfer(client->adapter,msgs, 2);
		if(ret == 2)break;
		retries++;
	}
	return ret;
}

/*******************************************************	
Description:
	write data to the flash slave device.

Parameter:
	client:	i2c device.
	buf[0]:operate address.
	buf[1]~buf[len]:write data buffer.
	len:operate length.
	
return:
	numbers of i2c_msgs to transfer.
*********************************************************/
static int BootLoader_write_bytes(struct i2c_client *client,uint8_t *data,int len)
{
	struct i2c_msg msg;
	int ret=-1;
	int retries = 0;

	msg.flags=client->flags;
	msg.addr=0x7F;//client->addr;
	msg.len=len;
	msg.buf=data;		
	msg.scl_rate = NOVATEK_I2C_SCL;
	while(retries<5)
	{
		ret=i2c_transfer(client->adapter,&msg, 1);
		if(ret == 1)break;
		retries++;
	}
	return ret;
}

/*******************************************************	
Description:
	Driver Bootloader  function.
return:
	Executive Outcomes. 0---succeed.
********************************************************/


void nvctp_DelayMs(unsigned long wtime)
{
	msleep(wtime);
}


unsigned char nvctp_InitBootloader(unsigned char bType,struct i2c_client *client)
{
	unsigned char ret = RS_OK;
    unsigned char status;
	unsigned char iic_buffer[13];
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	iic_buffer[0] = 0x00;
	if(bType == HW_RST)
	 {
		//nvctp_GobalRset(1,2);
		//nvctp_GobalRset(0,10);
		iic_buffer[1] = 0x00;
		i2c_write_bytes(ts->client, iic_buffer, 2);
		nvctp_DelayMs(2);
		//nvctp_GobalRset(1,2);				
	 }
	else if(bType == SW_RST)
	{
	    iic_buffer[1] = 0xA5;
		i2c_write_bytes(ts->client, iic_buffer, 2);
        nvctp_DelayMs(10);
		iic_buffer[1] = 0x00;
        i2c_write_bytes(ts->client, iic_buffer, 2);
        nvctp_DelayMs(2);
	}

	/*Read status*/
	i2c_read_bytes(ts->client,iic_buffer,2);
	printk("iic_buffer[1] %d\n",iic_buffer[1]);
	if(iic_buffer[1] != 0xAA)
	{
		ret = RS_INIT_ER;
	}
	printk("ret %d\n",ret);
	return ret;
}

/*************************************************************************************
*Description:
*			Flash Mass Erase Command(7FH --> 30H --> 00H)
*
*Return:
*			Executive Outcomes: 0 -- succeed
*************************************************************************************/
static int novatek_FlashEraseMass(struct i2c_client *client)
{
	uint8_t i,status;
	uint8_t Buffer[4]={0};
	int ret= RS_ERAS_ER;
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	
    Buffer[0] = 0x00;
    Buffer[1] = 0x33;
	for(i = 5; i > 0; i--)
	{
		Buffer[2] = 0x00;
		
		ret = BootLoader_write_bytes(ts->client, Buffer, 3);
		if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
		nvctp_DelayMs(25);

		/*Read status*/
   		ret = BootLoader_read_bytes(ts->client, Buffer, 2);
		if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
   		if(Buffer[1] == 0xAA)
   		{
	   		ret = RS_OK;
			break;
   		}
		
		nvctp_DelayMs(1);
	}
	return ret;


}
/*************************************************************************************
*
*
*
*
*
*************************************************************************************/
static int novatek_FlashEraseSector(struct i2c_client *client, uint16_t wAddress)
{
	uint8_t i,status;
	uint8_t Buffer[4]={0};
	int ret= RS_ERAS_ER;
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	
    Buffer[0] = 0x00;
    Buffer[1] = 0x30;
	for(i = 5; i > 0; i--)
	{
		Buffer[2] = (uint8_t)(wAddress >> 8);
		Buffer[3] = (uint8_t)(wAddress)&0x00FF;

		ret = BootLoader_write_bytes(ts->client, Buffer, 4);
		if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
		nvctp_DelayMs(10);

		/*Read status*/
   		ret = BootLoader_write_bytes(ts->client, Buffer, 2);
		if (ret <= 0)
    	{
			dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}
   		if(Buffer[1] == 0xAA)
   		{
	   		ret = RS_OK;
			break;
   		}
		
		nvctp_DelayMs(2);
	}
	return ret;


}

/*************************************************************************************
*
*
*
*
*
*************************************************************************************/
static int novatek_Initbootloader(uint8_t bType, struct i2c_client *client)
{
	u8 ret = RS_OK;
    u8 status;
	u8 iic_buffer[13];
	u8 write_cmd1[3] = {0xff,0xf1,0x91};
  	u8 write_cmd2[2] = {0x00,0x01};
	
	struct novatek_ts_data *ts = i2c_get_clientdata(client);

	
	//ts->client->Addr = 0x7F;
	iic_buffer[0] = 0x00;
	if(bType == HW_RST)
	 {
		iic_buffer[1] = 0x00;
		BootLoader_write_bytes(ts->client, (uint8_t *)iic_buffer, 2);
		nvctp_DelayMs(2);
		//nvctp_GobalRset(1,2);				
	 }
	else if(bType == SW_RST)
	{
	        iic_buffer[1] = 0xA5;
		BootLoader_write_bytes(ts->client, iic_buffer, 2);
                nvctp_DelayMs(10);
		iic_buffer[1] = 0x00;
		BootLoader_write_bytes(ts->client, (uint8_t *)iic_buffer, 2);
		nvctp_DelayMs(2);
	}

  /*Read status*/
   BootLoader_read_bytes(ts->client,iic_buffer,2);
   	printk("iic_buffer[1] %d\n",iic_buffer[1]);
   if(iic_buffer[1] != 0xAA)
   	{
	   ret = RS_INIT_ER;
   	}
   	printk("ret %d\n",ret);
	
  	//ts->client->Addr = 0x01;
    //i2c_write_bytes(ts->client, write_cmd1, 3); 
	//i2c_write_bytes(ts->client, write_cmd2, 2);
	//ts->client->Addr = 0x7F;
   return ret;
}

uint16_t nvctp_ReadDynamicChecksum(struct i2c_client *client)
{
	uint8_t  ret = RS_OK;
	uint8_t	 WriteCommand1[3]={0xFF, 0x8F, 0xFF};
	uint8_t  WriteCommand2[2]={0x00,0xE1};
	uint8_t	 WriteCommand3[3]={0xFF, 0x8E, 0x0E};
	uint8_t  ReadBuffer[5] = {0};
	uint16_t Checksum = 0xffff;

	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	struct novatek_platform_data *pdata = client->dev.platform_data;
	
	if(pdata->platform_wakeup)
	{
		pdata->platform_wakeup();
	}
	nvctp_DelayMs(800);  
	   	
	ret = i2c_write_bytes(ts->client, WriteCommand1,3);
	
	if (ret <= 0)
		{
	  	    dev_err(&(client->dev),"I2C transfer error. Number:%d\n ", ret);
		}
	ret = i2c_write_bytes(ts->client, WriteCommand2,2);
	
	if (ret <= 0)
    	{
      	    dev_err(&(client->dev),"I2C transfer error. Number:%d\n ", ret);
    	}	
	nvctp_DelayMs(500);
	nvctp_DelayMs(500);
	ret = i2c_write_bytes(ts->client, WriteCommand3,3);
	if (ret <= 0)
	{
  	    dev_err(&(client->dev),"I2C transfer error. Number:%d\n ", ret);
	}
	//ReadBuffer[0] = 0;
	nvctp_DelayMs(800);
	ret = i2c_read_bytes(ts->client, ReadBuffer,3);

	//memcpy(&cBuffer, &ReadBuffer, 3);
    Checksum = (uint16_t)(ReadBuffer[1] << 8)|(ReadBuffer[2]&0xFF);
	printk("=======DynCHECKSUM = 0x%x  \n",Checksum);
	if(ret <= 0)	
		{ 
		    dev_err(&(ts->client->dev),"I2C transfer error. Number:%d\n ", ret);	
		}
	return Checksum;	
}

/*******************************************************************************************





********************************************************************************************/
unsigned char nvctp_ReadFinalChecksum(unsigned long flash_addr, unsigned long final_checksum,struct i2c_client *client)
{
	unsigned char iic_data_buffer[14]={0};
	unsigned long wValue,DynCheckSum;
  	unsigned char ret = RS_OK;
  	
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	
	DynCheckSum = nvctp_ReadDynamicChecksum(ts->client);
	printk("%d  =======DynCheckSum = 0x%x  \n",__LINE__,DynCheckSum);
	printk("%d  =======final_checksum = 0x%x  \n",__LINE__,final_checksum);
	if(DynCheckSum == final_checksum)
	{
	    return (ret = RS_OK);	
	}	
	
	/*inital bootloader*/
	ret = nvctp_InitBootloader(SW_RST,ts->client);
	if(ret != RS_OK)
	 {
		return ret;
	 }
	iic_data_buffer[0]=0x00;	
	
	iic_data_buffer[1] = 0x99;
	iic_data_buffer[2] = (unsigned char)(flash_addr >> 8);
	iic_data_buffer[3] = flash_addr&0xFF;
	iic_data_buffer[4] = 8;
	BootLoader_write_bytes(ts->client,iic_data_buffer,5);
	nvctp_DelayMs(2);
	BootLoader_read_bytes(ts->client,iic_data_buffer,14);
	wValue = (unsigned long)iic_data_buffer[12]<<8 |iic_data_buffer[13];

	if((wValue != final_checksum)||(DynCheckSum != final_checksum))
	{
		ret = RS_FLCS_ER;
	}

	return ret;
}


/*************************************************************************************
*
*
*
*
*
*************************************************************************************/
////////////////////////////////////////////////////////////////////////////////////
static int nvctp_WriteDatatoFlash( unsigned char *fw_BinaryData, unsigned long BinaryDataLen,struct i2c_client *client)
{
	uint8_t ret = RS_OK;
  	uint8_t iic_data_buffer[14];
	uint8_t j,k;
	uint8_t iic_buffer[16] ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	uint8_t Checksum[16] ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint16_t flash_addr;
	uint16_t sector;
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	struct novatek_platform_data *pdata = client->dev.platform_data;

	printk("--%s--\n",__func__);
	WFStart:
				iic_data_buffer[0]=0x00;
				sector = 0;
				flash_addr = 0;

				for(sector = 0; sector < FLASHSECTORSIZE; sector++)
				{
	WFRepeat:	    	
					printk("data writing ....... %d \n",sector);   	 
     	 			flash_addr = 128*sector;  
     	 			for (j = 0; j < 16; j++)
     	 			{
        				/* Write Data to flash*/
						iic_data_buffer[1] = 0x55;
						iic_data_buffer[2] = (unsigned char)(flash_addr >> 8);
						iic_data_buffer[3] = (unsigned char)flash_addr;
		    			iic_data_buffer[4] = 8;
		    
		    			iic_data_buffer[6] = fw_BinaryData[flash_addr + 0];
		    			iic_data_buffer[7] = fw_BinaryData[flash_addr + 1];
		    			iic_data_buffer[8] = fw_BinaryData[flash_addr + 2];
		    			iic_data_buffer[9] = fw_BinaryData[flash_addr + 3];
		    			iic_data_buffer[10]= fw_BinaryData[flash_addr + 4];
		    			iic_data_buffer[11]= fw_BinaryData[flash_addr + 5];
		    			iic_data_buffer[12]= fw_BinaryData[flash_addr + 6];
		    			iic_data_buffer[13]= fw_BinaryData[flash_addr + 7];
        
		    			Checksum[j] = ~(iic_data_buffer[2]+iic_data_buffer[3]+iic_data_buffer[4]+iic_data_buffer[6]+\
		    	    	iic_data_buffer[7]+iic_data_buffer[8]+iic_data_buffer[9]+\
		    	    	iic_data_buffer[10]+iic_data_buffer[11]+iic_data_buffer[12]+iic_data_buffer[13]) + 1;
		    			iic_data_buffer[5] = Checksum[j];
	    
		    			BootLoader_write_bytes(ts->client, iic_data_buffer, 14);

						flash_addr += 8;	
     	 			}
					nvctp_DelayMs(20);
					/*Setup force genrate Check sum */
    				flash_addr = 128*sector;  
    				for (j = 0; j < 16; j++)
    				{
                		iic_data_buffer[0] = 0x00;
						iic_data_buffer[1] = 0x99;
						iic_data_buffer[2] = (unsigned char)(flash_addr >> 8);
      	        		iic_data_buffer[3] = (unsigned char)flash_addr & 0xFF;
		        		iic_data_buffer[4] = 8;
						BootLoader_write_bytes(ts->client, iic_data_buffer,5);
						nvctp_DelayMs(2);
						BootLoader_read_bytes(ts->client, iic_data_buffer, 14);
	                  
						if(iic_data_buffer[5] != Checksum[j] )
						{
							printk("checksum error iic_data_buffer[5] = %x,  Checksum[%d] = %x\n", iic_data_buffer[5],j,Checksum[j]);
			  				ret = RS_WD_ER;

            				//novatek_FlashEraseMass(ts->client);
            				novatek_FlashEraseSector(ts->client,(unsigned int)(sector*128));
            				goto WFRepeat;
						}
						flash_addr += 8;	
	                    nvctp_DelayMs(2);
	          }


   }
   return ret;		        
}
/*************************************************************************************
*
*
*
*
*
*************************************************************************************/
static int novatek_Bootloader(unsigned char *nvctp_binaryfile , unsigned long nvctp_binaryfilelength,struct i2c_client *client)
{
	uint8_t i;
	uint8_t ret = RS_OK;
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
        
	printk("--%s--\n",__func__);
	
	/*Erase Sector*/
	ret = novatek_FlashEraseMass(ts->client);

	if(ret != RS_OK)
	{
		printk("nvctp_EraseSector error \n");
		return ret;
	}
	/*Write binary data to flash*/
	ret = nvctp_WriteDatatoFlash(nvctp_binaryfile,nvctp_binaryfilelength,ts->client);
	if(ret != RS_OK)
	{
		return ret;
	}
	
    printk("--%s-- ret = %d\n",__func__,ret);
	return ret;
}

/*************************************************************************************
*
*
*
*
*
*************************************************************************************/
static int	nvctp_CheckIsBootloader(struct i2c_client *client)
{
	struct novatek_ts_data *ts = i2c_get_clientdata(client);
	struct novatek_platform_data *pdata = client->dev.platform_data;
	uint16_t FW_CHECKSUM ;
	uint8_t iic_buffer[3];
	printk("nvctp_CheckIsBootloader\n");
		   
	FW_CHECKSUM = (unsigned int)(nvctp_BinaryFile[FW_DATASIZE-2]<< 8 |nvctp_BinaryFile[FW_DATASIZE-1]);
	printk("%s,  new version checksum = %x \n",__func__,FW_CHECKSUM );
	if(nvctp_ReadFinalChecksum(FW_CHECKSUM_ADDR,FW_CHECKSUM,ts->client))
		{
			printk("nvctp_CheckIsBootloader1\n");
			novatek_Bootloader(nvctp_BinaryFile,sizeof(nvctp_BinaryFile),ts->client);
		}
		else
		{
			printk("--%s--, tp version is no change\n",__func__);
			iic_buffer[0] = 0x00;
			iic_buffer[1] = 0xA5;
			BootLoader_write_bytes(ts->client, iic_buffer, 2);										
		}
	
	/*****Hareware reset command******/
	pdata->platform_wakeup();
}


/*************************************************************************/

#endif

static const struct i2c_device_id novatek_ts_id[] = {
	{ NOVATEK_I2C_NAME, 0 },
	{ }
};

static struct i2c_driver novatek_ts_driver = {
	.probe		= novatek_ts_probe,
	.remove		= novatek_ts_remove,
//	.resume		= novatek_ts_resume,
//	.suspend		= novatek_ts_suspend,
	.id_table	= novatek_ts_id,
	.driver = {
		.name	= NOVATEK_I2C_NAME,
		.owner = THIS_MODULE,
	},
};

/*******************************************************	
Description:
	Driver Install function.
return:
	Executive Outcomes. 0---succeed.
********************************************************/
static int __devinit novatek_ts_init(void)
{
	int ret;
	
	//novatek_wq = create_workqueue("novatek_wq");		//create a work queue and worker thread
	novatek_wq = create_singlethread_workqueue("novatek_wq");
	if (!novatek_wq) {
		printk(KERN_ALERT "creat workqueue faiked\n");
		return -ENOMEM;
		
	}
	ret=i2c_add_driver(&novatek_ts_driver);
	return ret; 
}

/*******************************************************	
Description:
	Driver uninstall function.
return:
	Executive Outcomes. 0---succeed.
********************************************************/
static void __exit novatek_ts_exit(void)
{
	printk(KERN_ALERT "Touchscreen driver of guitar exited.\n");
	i2c_del_driver(&novatek_ts_driver);
	if (novatek_wq)
		destroy_workqueue(novatek_wq);		//release our work queue
}
late_initcall(novatek_ts_init);
module_exit(novatek_ts_exit);

MODULE_DESCRIPTION("Novatek Touchscreen Driver");
MODULE_LICENSE("GPL");


