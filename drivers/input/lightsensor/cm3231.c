/* drivers/input/lightsensor/cm3231.c
 *
 * Copyright (C) 2011 ROCK-CHIPS, Inc.
 * Author: yxj <yxj@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/irq.h>
#include <linux/gpio.h>
#include <linux/input.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/circ_buf.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/wait.h>

#include "cm3231.h"

#if 1
#define DBG(x...) printk(KERN_INFO x)
#else
#define DBG(x...) do { } while (0)
#endif
	 
static int dbg_thresd = 0;
module_param(dbg_thresd, int, S_IRUGO|S_IWUSR);

//#define DBG(x...) do { if(unlikely(dbg_thresd)) printk(KERN_INFO x); } while (0)

#define cm3231_I2C_SPEED 100*1000
#define I2C_RETRY_COUNT 10
  
struct cm3231_info *g_lp_info;
static struct mutex als_enable_mutex;

 static int I2C_RxData(uint16_t slaveAddr, uint8_t *rxData, int length)
 {
	 uint8_t loop_i;
	 struct cm3231_info *lpi = g_lp_info;
 
	 struct i2c_msg msgs[] = {
		 {
		  .addr = slaveAddr,
		  .flags = I2C_M_RD,
		  .len = length,
		  .buf = rxData,
		  .scl_rate = cm3231_I2C_SPEED,
		  },
	 };
 
	 for (loop_i = 0; loop_i < I2C_RETRY_COUNT; loop_i++) {
 
		 if (i2c_transfer(lpi->i2c_client->adapter, msgs, 1) > 0)
			 break;
		 msleep(10);
	 }
	 if (loop_i >= I2C_RETRY_COUNT) {
		 printk(KERN_ERR "[cm3231 error] %s retry over %d\n",
			 __func__, I2C_RETRY_COUNT);
		 return -EIO;
	 }
 
	 return 0;
 }
 
 static int I2C_TxData(uint16_t slaveAddr, uint8_t *txData, int length)
 {
	 uint8_t loop_i;
	 struct cm3231_info *lpi = g_lp_info;
 
	 struct i2c_msg msg[] = {
		 {
		  .addr = slaveAddr,
		  .flags = 0,
		  .len = length,
		  .buf = txData,
		  .scl_rate = cm3231_I2C_SPEED,
		  },
	 };

	 for (loop_i = 0; loop_i < I2C_RETRY_COUNT; loop_i++) {
		 if (i2c_transfer(lpi->i2c_client->adapter, msg, 1) > 0)
			 break;
		 msleep(10);
	 }
 
	 if (loop_i >= I2C_RETRY_COUNT) {
		 printk(KERN_ERR "[cm3231 error] %s retry over %d\n",
			 __func__, I2C_RETRY_COUNT);
		 return -EIO;
	 }
 
	 return 0;
 }
 
 
 static int _cm3231_I2C_Read_Byte(uint16_t slaveAddr, uint8_t *pdata)
 {
	 uint8_t buffer = 0;
	 int ret = 0;
 
 	 DBG("zpp-->traced:%s-->%d\n",__func__,__LINE__);
	 if (pdata == NULL)
		 return -EFAULT;
 
     DBG("zpp-->traced:%s-->%d  pdata!=NULL\n",__func__,__LINE__);
     
	 ret = I2C_RxData(slaveAddr, &buffer, 1);
	 if (ret < 0) {
		 pr_err(
			 "[cm3231 error]%s: I2C_RxData fail, slave addr: 0x%x\n",
			 __func__, slaveAddr);
		 return ret;
	 }
 
	 *pdata = buffer;
	 
	 return ret;
 }
 
 static int _cm3231_I2C_Write_Byte(uint16_t SlaveAddress, uint8_t data)
 {
	 uint8_t buffer[2];
	 int ret = 0;
	 buffer[0] = data;

	 ret = I2C_TxData(SlaveAddress, &buffer[0], 1);
	 if (ret < 0) {
		 pr_err("[cm3231 error]%s: I2C_TxData fail\n", __func__);
		 return -EIO;
	 }
 
	 return ret;
 }
 
static void report_lsensor_input_event(struct cm3231_info *lpi)
{/*when resume need report a data, so the paramerter need to quick reponse*/
	 uint8_t level = 0, i;
	 //printk("========the als_code is %d\n",lpi->als_code);
	 for (i = 0; i < LEVELS_NUM; i++) {
		 if (lpi->als_code<=(*(lpi->adc_table + i))) {
			 level = i-1;
			 if (*(lpi->adc_table + i))
				 break;
		 }
		 if ( i == LEVELS_NUM-1) {
			 level = i;
			 break;
		 }
	 }
	
	input_report_abs(lpi->ls_input_dev, ABS_MISC, level);
	input_sync(lpi->ls_input_dev);
	DBG("report light sensor value:%d>>level:%d\n",lpi->als_code,level);
 
 }

 static int cm3231_enable(struct cm3231_info *lpi)
 {
 	int ret;
	printk("%s>>>>\n",__func__);
	if(!lpi->als_enable)  //check cm3231 enable or not
	{
		mutex_lock(&als_enable_mutex);
		//ret = _cm3231_I2C_Write_Byte(CMD_REG2, FD_IT800);
		//if(ret < 0)
		//{
		//	printk("%s>>>CMD_REG2 write fail\n",__func__);
		//	goto out;
		//}
		ret = _cm3231_I2C_Write_Byte(CMD_REG1, REG1_FD_IT200);
		if(ret < 0)
		{
			printk("%s>>>CMD_REG1 write fail\n",__func__);
			goto out;
		}
	
		ret = _cm3231_I2C_Write_Byte(CMD_REG2, GEG2_Defval);
		if(ret < 0)
		{
			printk("%s>>>CMD_REG2 write fail\n",__func__);
			goto out;
		}
		ret = _cm3231_I2C_Write_Byte(CMD_REG3, FD_IT_TIMES1);
		if(ret < 0)
		{
			printk("%s>>>CMD_REG1 write fail\n",__func__);
			goto out;
		}
		lpi->als_enable = 1;
		queue_delayed_work(lpi->lp_wq, &lpi->ls_work,HZ);
	}
	else
	{
		return 0;
	}
out:
	mutex_unlock(&als_enable_mutex);
	return 0;
 }

 static int cm3231_disable(struct cm3231_info *lpi)
 {
 	int ret ;
	printk("%s>>>>\n",__func__);
	if(lpi->als_enable)
	{
		mutex_lock(&als_enable_mutex);
		DBG("zpp-->trace--->%s---line:%d\n",__func__,__LINE__);
		cancel_delayed_work_sync(&lpi->ls_work);
		ret = _cm3231_I2C_Write_Byte(CMD_REG1, SD);  //disable cm3231
		if(ret < 0)
		{
			printk("%s>>>CMD_REG1 write fail\n",__func__);
			goto out;
		}
		lpi->als_enable = 0;
	}
	else
	{
		return 0;
	}
out:
	mutex_unlock(&als_enable_mutex);
	return 0;
 }
 
 static int lightsensor_open(struct inode *inode, struct file *file)
 {
 	
	struct cm3231_info *lpi = g_lp_info;
	int rc = 0;
	DBG("zpp-->trace ---> %s\n",__func__);
	if (lpi->lightsensor_opened) {
		 pr_err("[cm3231 error]%s: already opened\n", __func__);
		 rc = -EBUSY;
	 }
	 lpi->lightsensor_opened = 1;
	 return rc;
 }
 
 static int lightsensor_release(struct inode *inode, struct file *file)
 {
	 struct cm3231_info *lpi = g_lp_info;
 
	 lpi->lightsensor_opened = 0;
	 return 0;
 }
 
 static long lightsensor_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
 {
	 int rc, val;
	 struct cm3231_info *lpi = g_lp_info;
 
 	 printk("zpp-->trace:%s-->cmd=%d\n",__func__,cmd);
	 switch (cmd) {
	 case LIGHTSENSOR_IOCTL_ENABLE:
	 	printk("zpp-->trace:%s-->cmd=LIGHTSENSOR_IOCTL_ENABLE\n",__func__);
		 if (get_user(val, (unsigned long __user *)arg)) {
			 rc = -EFAULT;
			 break;
		 }
		 rc = val ? cm3231_enable(lpi) : cm3231_disable(lpi);
		 break;
	 case LIGHTSENSOR_IOCTL_GET_ENABLED:
	 	printk("zpp-->trace:%s-->cmd=LIGHTSENSOR_IOCTL_GET_ENABLED\n",__func__);
		 val = lpi->als_enable;
		 rc = put_user(val, (unsigned long __user *)arg);
		 break;
	 default:
		 pr_err("[cm3231 error]%s: invalid cmd %d\n",
			 __func__, _IOC_NR(cmd));
		 rc = -EINVAL;
	 }
 
	 return rc;
 }
 
 static const struct file_operations lightsensor_fops = {
	 .owner = THIS_MODULE,
	 .open = lightsensor_open,
	 .release = lightsensor_release,
	 .unlocked_ioctl = lightsensor_ioctl
 };
 
 static struct miscdevice lightsensor_misc = {
	 .minor = MISC_DYNAMIC_MINOR,
	 .name = "lightsensor",
	 .fops = &lightsensor_fops
 };
  
 static int lightsensor_setup(struct cm3231_info *lpi)
 {
	 int ret;
 
	 lpi->ls_input_dev = input_allocate_device();
	 if (!lpi->ls_input_dev) {
		 pr_err(
			 "[cm3231 error]%s: could not allocate ls input device\n",
			 __func__);
		 return -ENOMEM;
	 }
	 lpi->ls_input_dev->name = "lightsensor-level";
	 set_bit(EV_ABS, lpi->ls_input_dev->evbit);
	 input_set_abs_params(lpi->ls_input_dev, ABS_MISC, 0, 9, 0, 0);
 
	 ret = input_register_device(lpi->ls_input_dev);
	 if (ret < 0) {
		 pr_err("[cm3231 error]%s: can not register ls input device\n",
				 __func__);
		 goto err_free_ls_input_device;
	 }
 
	 ret = misc_register(&lightsensor_misc);
	 if (ret < 0) {
		 pr_err("[cm3231 error]%s: can not register ls misc device\n",
				 __func__);
		 goto err_unregister_ls_input_device;
	 }
 
	 return ret;
 
 err_unregister_ls_input_device:
	 input_unregister_device(lpi->ls_input_dev);
 err_free_ls_input_device:
	 input_free_device(lpi->ls_input_dev);
	 return ret;
 } 

static void  rk_ls_work_func(struct work_struct *pwork)
{	
	int ret;
	uint8_t lsb,msb;
	uint16_t als_code;
	struct cm3231_info *lpi = container_of(to_delayed_work(pwork), struct cm3231_info, ls_work);
	if(lpi->als_enable)
	{
		//printk("enter rk_ls_work_func=====\n");
		ret = _cm3231_I2C_Read_Byte(DATA_REG23, &lsb);
		if(ret < 0)
		{
			printk(KERN_ERR "read data register 23 error:%d\n",ret);
		}
		ret = _cm3231_I2C_Read_Byte(DATA_REG21, &msb);
		if(ret < 0)
		{
			printk(KERN_ERR "read data register 21 error:%d\n",ret);
		}
		
		als_code = msb;
		als_code <<=8;
		als_code |=lsb;
		DBG("zpp-->traced:%s--->%d--als_code=%d\n",__func__,__LINE__,als_code);
		lpi->als_code = als_code*160;//*715;
		DBG("zpp-->traced:%s--->%d--lpi->als_code=%d\n",__func__,__LINE__,lpi->als_code);
		report_lsensor_input_event(lpi);
		queue_delayed_work(lpi->lp_wq, &lpi->ls_work,HZ);  //every 1s update once
	}
	
}

static void cm3231_early_suspend(struct early_suspend *h)
{
	struct cm3231_info *lpi = g_lp_info;
	cm3231_disable(lpi);
	
}
 
static void cm3231_late_resume(struct early_suspend *h)
{
	struct cm3231_info *lpi = g_lp_info;
	cm3231_enable(lpi);
}
 
static int cm3231_probe(struct i2c_client *client,const struct i2c_device_id *id)
{
	 int ret = 0;
	 struct cm3231_info *lpi;
	 struct cm3231_platform_data *pdata;
	 uint8_t lsb,msb;
 
	 lpi = kzalloc(sizeof(struct cm3231_info), GFP_KERNEL);
	 if (!lpi)
		 return -ENOMEM;
 
	 lpi->i2c_client = client;
	 pdata = client->dev.platform_data;
	 if (!pdata) {
		 pr_err("[cm3231 error]%s: Assign platform_data error!!\n", __func__);
		 ret = -EBUSY;
		 goto err_platform_data_null;
	 }

	 lpi->adc_table = pdata->levels;
	 i2c_set_clientdata(client, lpi);
	g_lp_info = lpi;
	
//	while(1)
//	 {
//	DBG("zpp-->trace-->_cm3231_I2C_Write_Byte    111111111111\n");
//	 ret = _cm3231_I2C_Write_Byte(CMD_REG1, REG1_FD_IT200);
//	 if(ret < 0)
//	 {
//		printk(KERN_ERR "read data register 23 error:%d\n",ret);
//	 }
//	 DBG("zpp-->trace-->_cm3231_I2C_Write_Byte\n");
//	}
	 
	 ret = lightsensor_setup(lpi);
	 if (ret < 0) {
		 pr_err("[cm3231 error]%s: lightsensor_setup error!!\n",
			 __func__);
		 goto err_platform_data_null;
	 }
 
	 lpi->lp_wq = create_singlethread_workqueue("cm3231_wq");
	 if (!lpi->lp_wq) {
		 pr_err("[cm3231 error]%s: can't create workqueue\n", __func__);
		 ret = -ENOMEM;
		 goto err_create_singlethread_workqueue;
	 }
 	 INIT_DELAYED_WORK(&lpi->ls_work, rk_ls_work_func);
	 mutex_init(&als_enable_mutex);

	 
	 
	 lpi->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN + 1;
	 lpi->early_suspend.suspend = cm3231_early_suspend;
	 lpi->early_suspend.resume = cm3231_late_resume;
	 register_early_suspend(&lpi->early_suspend);
	 printk(KERN_INFO "cm3231 Probe success!\n");
 	
	 
	 return ret;

 err_create_singlethread_workqueue:
 	 destroy_workqueue(lpi->lp_wq);
 err_platform_data_null:
  	 kfree(lpi);
  	 return ret;
}

static int __devexit cm3231_remove(struct i2c_client *client)
{
	struct cm3231_info *lpi =  i2c_get_clientdata(client);
	#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&lpi->early_suspend);
	#endif
	input_unregister_device(lpi->ls_input_dev);
	destroy_workqueue(lpi->lp_wq);
	i2c_set_clientdata(client, NULL);
	kfree(lpi);
	return 0;
}

static void cm3231_shutdown(struct i2c_client *client)
{
    struct cm3231_info *lpi =  i2c_get_clientdata(client);
#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&lpi->early_suspend);
#endif
	kfree(lpi);
}

static const struct i2c_device_id cm3231_i2c_id[] = {
	{"lightsensor", 0},
	{ }
};

static struct i2c_driver cm3231_i2c_driver = {
	.driver = {
		.name = CM3231_DRV_NAME,
		.owner = THIS_MODULE,
	},
	.probe    = cm3231_probe,
	.shutdown = cm3231_shutdown,
	.remove   = __devexit_p(cm3231_remove),
	.id_table = cm3231_i2c_id,
};

static int __init cm3231_init(void)
{
	return i2c_add_driver(&cm3231_i2c_driver);
}

static void __exit cm3231_exit(void)
{
	i2c_del_driver(&cm3231_i2c_driver);
}

module_init(cm3231_init);
module_exit(cm3231_exit);

MODULE_AUTHOR("yxj@rock-chips.com");
MODULE_DESCRIPTION("cm3231 light sensor driver");
MODULE_LICENSE("GPL");
