#if !defined (__zpp_debug_trace_h__)
#define __zpp_debug_trace_h__


#if !defined (zpplicate_debug_trace)

#define zpplicate_debug_trace 1

#endif /* #if !defined (zpplicate_debug_trace) */

#if zpplicate_debug_trace 

#define DBG_Trace(x...)  \
 printk("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");\
 printk("$$$$-->[ DBG_Trace ]--> \t");\
 printk(x);\
 printk("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n\n")

#else

#define DBG_Trace(x...) do { } while (0)

#endif

#endif //#if !defined (__zpp_debug_trace_h__)
