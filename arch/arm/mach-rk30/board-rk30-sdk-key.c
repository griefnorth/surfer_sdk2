#include <mach/gpio.h>
#include <plat/key.h>

#define EV_ENCALL				KEY_F4
#define EV_MENU					KEY_F1

#define PRESS_LEV_LOW			1
#define PRESS_LEV_HIGH			0

static struct rk29_keys_button key_button[] = {
	{
		.desc	= "play",
		.code	= KEY_POWER,
		.gpio	= RK30_PIN6_PA2,
		.active_low = PRESS_LEV_LOW,
		//.code_long_press = EV_ENCALL,
		.wakeup	= 1,
	},
#if defined(CONFIG_LCD_SC_N81S) || defined(CONFIG_LCD_SC_N71SI)
	{
		.desc	= "vol-",
		.code	= KEY_BACK,
		.code_long_press = KEY_VOLUMEUP,//KEY_VOLUMEDOWN,
		.gpio	= RK30_PIN4_PC5,
		.active_low = PRESS_LEV_LOW,
	},
	{
		.desc	= "vol+",
		.code	= KEY_HOME,
		.code_long_press = KEY_VOLUMEDOWN,//KEY_VOLUMEUP,
		.adc_value	= 1,
		.gpio = INVALID_GPIO,
		.active_low = PRESS_LEV_LOW,
	},
#elif defined(CONFIG_LCD_SC_S12)
	{
		.desc	= "vol-",
		.code	= KEY_VOLUMEDOWN,//KEY_BACK,
//		.code_long_press = KEY_VOLUMEDOWN,
		.gpio	= RK30_PIN4_PC5,
		.active_low = PRESS_LEV_LOW,
	},
	{
		.desc	= "vol+",
		.code	= KEY_VOLUMEUP,//KEY_HOME,
//		.code_long_press = KEY_VOLUMEUP,
		.adc_value	= 1,
		.gpio = INVALID_GPIO,
		.active_low = PRESS_LEV_LOW,
	},
#elif defined(CONFIG_LCD_SC_N71S)
	{
		.desc	= "vol+",
		.code	= KEY_BACK,
		.code_long_press = KEY_VOLUMEUP,
		.gpio = RK30_PIN4_PC5,//INVALID_GPIO,
		.active_low = PRESS_LEV_LOW,
	},
	{
		.desc	= "vol-",
		.code	= EV_MENU,
		.code_long_press = KEY_VOLUMEDOWN,
		.gpio	= INVALID_GPIO,//RK30_PIN4_PC5,
		.adc_value	= 1,
		.active_low = PRESS_LEV_LOW,
	},	
#else
	{
		.desc	= "vol+",
		.code	= KEY_HOME,
		.code_long_press = KEY_VOLUMEUP,
		.gpio = RK30_PIN4_PC5,//INVALID_GPIO,
		.active_low = PRESS_LEV_LOW,
	},
	{
		.desc	= "vol-",
		.code	= KEY_BACK,
		.code_long_press = KEY_VOLUMEDOWN,
		.gpio	= INVALID_GPIO,//RK30_PIN4_PC5,
		.adc_value	= 1,
		.active_low = PRESS_LEV_LOW,
	},
#endif	
};
struct rk29_keys_platform_data rk29_keys_pdata = {
	.buttons	= key_button,
	.nbuttons	= ARRAY_SIZE(key_button),
	.chn	= 1,  //chn: 0-7, if do not use ADC,set 'chn' -1
};

